/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/**
 *  @File   input_pad.c
 *  @brief
 *
 *
 *
 * */

#include "stm_selector.h"
#include "switch_global.h"
#include "selector.h"
#include "input_pad.h"
#include <stm_event.h>


#define SWITCH_CLASS_IN_PAD_MAX	100
#define MAX_GOP_CACHED 7
#define MIN_FREE_SPACE 200000
#define MAX_PCR 0x1FFFFFFFF
#define MAX_PCR_JUMP (90 * 1000)

#define TimeIsBefore(V, R) ((V < R) ? ((R - V) < (MAX_PCR - R + V)) :\
				((V - R) > (MAX_PCR - V + R)))
#define PcrLimit(V) ((V) & MAX_PCR)
#define InTimePeriod(V, S, E) ((E < S) ? ((V >= S) || (V < E)) : \
				((V >= S) && (V < E)))

static int input_pad_attach(struct switch_obj *src_object,
		stm_object_h sink_object);
static int input_pad_detach(struct switch_obj *src_object,
		stm_object_h sink_object);
static int input_pad_data_push_attach(stm_object_h src_object,
		stm_object_h sink_object);
static int input_pad_data_push_detach(stm_object_h src_object,
		stm_object_h sink_object);
static int input_pad_clk_push_attach(stm_object_h src_object,
		stm_object_h sink_object);
static int input_pad_clk_push_detach(stm_object_h src_object,
		stm_object_h sink_object);

static int input_pad_set_clock_data_point(stm_se_playback_h obj,
				stm_se_time_format_t time_format,
				bool new_seq,
				unsigned long long source_time,
				unsigned long long system_time);

static int input_pad_push_data(stm_object_h input_pad,
				struct stm_data_block *block_list,
				uint32_t block_count,
				uint32_t *data_blocks);
static int input_pad_set_secure_path_status(stm_object_h pad_h,
				bool secure_flag);

static struct input_pad_s  *get_input_pad_from_obj(struct switch_obj *pad_obj);
static int __send_pcr_to_associated_in_pads(struct input_pad_s *input_pad,
					unsigned long long pcr);
static int input_pad_delete(struct switch_obj *pad_obj);
static int input_pad_set_control(struct switch_obj *pad_obj, uint32_t control,
				const void *buf, uint32_t size);
static int input_pad_get_control(struct switch_obj *pad_obj, uint32_t control,
				void *buf, uint32_t size);


static int __set_connect_mode(struct input_pad_s *pad,
			int new_rebase_policy);

static int __update_pad_connect_mode(struct input_pad_s *pad,
		struct switch_obj *sel_obj);

static int __update_rebase_policy(struct input_pad_s *pad,
			int new_rebase_policy);

static int __cache_data(struct input_pad_s *in_pad,
		struct stm_data_block *block_list,
		uint32_t block_count, uint32_t *data_blocks);

static int __cache_audio_data(struct input_pad_s *in_pad,
		struct stm_data_block *block_list,
		uint32_t block_count, uint32_t *data_blocks);

static void __update_gop_pts(struct input_pad_s *in_pad);
static void __rebase_single_gop(struct input_pad_s *in_pad, uint32_t *bytes);
static void __update_caching_status(struct input_pad_s *in_pad);
static int __cache_video_data(struct input_pad_s *in_pad,
		struct stm_data_block *block_list,
		uint32_t block_count, uint32_t *data_blocks);

static inline int __reject_this_push(struct stm_data_block *block_list,
		uint32_t block_count, uint32_t *data_blocks);
static inline struct cache_node_s *__alloc_cache_node(
			struct input_pad_s *in_pad);
static int __locate_pes_start(struct input_pad_s *in_pad,
			struct stm_data_block *block_list,
			uint32_t block_count);
static inline int __cp_pes_sync_bytes(struct input_pad_s *pad, 
			uint8_t *pes_holder);

static int __process_pes_for_caching(struct input_pad_s *in_pad,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks);

static int __parse_pes_header(struct input_pad_s *in_pad,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks);
static int __empty_cache_list(struct input_pad_s *in_pad);
static int __reject_all_cached_data(struct input_pad_s *in_pad,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks);

static void __cp_src_buf_info(struct input_pad_s *in_pad,
			struct stm_data_block *pad_blk,
			struct stm_data_block *src_blk,
			uint32_t offset);
static int __is_push_duplicate(struct input_pad_s *in_pad,
		struct stm_data_block *blk_list, uint32_t blk_cnt);

static stm_data_interface_push_sink_t in_pad_data_push_if = {
	input_pad_data_push_attach, /* connect field */
	input_pad_data_push_detach, /* disconnect field */
	input_pad_push_data, /* push_data field */
	KERNEL, /* mem_type field */
	STM_IOMODE_NON_BLOCKING_IO, /* mode field */
	0, /* alignement field */
	0, /* max_transfer field */
	0, /* paketized field */
	input_pad_set_secure_path_status
};

static stm_se_clock_data_point_interface_push_sink_t in_pad_clk_push_if = {
	input_pad_clk_push_attach, /* connect field */
	input_pad_clk_push_detach, /* disconnect field */
	input_pad_set_clock_data_point /* set_clock_data_point_data field */
};

static struct switch_obj_ops input_pad_ops = {
	.delete = &input_pad_delete,
	.set_control = &input_pad_set_control,
	.get_control = &input_pad_get_control,
	.attach = &input_pad_attach,
	.detach = &input_pad_detach,
};

static struct input_pad_s  *get_input_pad_from_obj(struct switch_obj *pad_obj)
{
	enum switch_obj_class_e obj_class;

	if (!pad_obj) {
		pr_notice("%s:%d> Invalid pad_obj: 0x%p\n", __func__,
					__LINE__, pad_obj);
		return NULL;
	}

	obj_class = get_class_type_from_obj(pad_obj);
	if (obj_class != SWITCH_OBJ_CLASS_INPUT_PAD) {
		pr_notice("%s:%d> pad_obj: 0x%p obj_class:%d\n",
				__func__, __LINE__, pad_obj, obj_class);
		return NULL;
	}
	return container_of(pad_obj, struct input_pad_s,
				obj);
}

int get_in_pad_from_hdl(stm_object_h pad, struct input_pad_s **input_pad)
{
	struct switch_obj_class *obj_class = NULL;
	int ret = 0;
	struct switch_obj *pad_obj;

	obj_class = get_class_from_class_type(SWITCH_OBJ_CLASS_INPUT_PAD);
	ret = get_obj_from_hdl(pad, obj_class, &pad_obj);
	if (ret)
		return ret;

	*input_pad = get_input_pad_from_obj(pad_obj);

	ret = (*input_pad == NULL) ? -EINVAL : 0;

	return ret;
}

int input_pad_register_class(struct switch_obj_class *input_pad_class)
{
	int err = 0;

	err = stm_registry_add_object(STM_REGISTRY_TYPES,
			SWITCH_CLASS_INPUT_PAD,
			input_pad_class);
	if (err) {
		pr_err("Failed to register input pad class\n");
		goto __error_add_obj;
	}
	err = stm_registry_add_attribute(input_pad_class,
			STM_DATA_INTERFACE_PUSH, STM_REGISTRY_ADDRESS,
			&in_pad_data_push_if,
			sizeof(in_pad_data_push_if));
	if (err) {
		pr_err("Failed to register interface %s\n",
				STM_DATA_INTERFACE_PUSH);
		goto __error_add_attr;
	}

	err = stm_registry_add_attribute(input_pad_class,
			STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH,
			STM_REGISTRY_ADDRESS,
			&in_pad_clk_push_if,
			sizeof(in_pad_clk_push_if));
	if (err) {
		pr_err("Failed to register interface %s\n",
				STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH);
		goto __error_add_attr;
	}

	input_pad_class->class = SWITCH_OBJ_CLASS_INPUT_PAD;
	input_pad_class->max = SWITCH_CLASS_IN_PAD_MAX;
	atomic_set(&input_pad_class->count, 0);

	return 0;

__error_add_attr:
	err = stm_registry_remove_object(input_pad_class);

__error_add_obj:
	return err;
}


int input_pad_unregister_class(struct switch_obj_class *input_pad_class)
{
	int err = 0;

	err = stm_registry_remove_object(input_pad_class);
	if (err) {
		pr_err("Failed to unregister input pad class\n");
		goto __error;
	}

	return 0;

__error:
	return err;
}


/*!
 * \brief Creates a new Input Pad object
 *
 * \param sel_obj  Parent selector for new PAD object
 * \param pad_obj Set to point to the new PAD object on success
 *
 * \retval 0       Success
 * \retval -EINVAL A bad parameter was supplied
 * \retval -ENOMEM Insufficient resources to allocate the new pad
 */
int input_pad_new(struct switch_obj *sel_obj, struct switch_obj **pad_obj)
{
	struct input_pad_s  *pad;
	struct stm_selector_s *sel = NULL;
	struct switch_obj_class *input_pad_class;
	int err = 0;
	char name[STM_REGISTRY_MAX_TAG_SIZE];

	sel = get_selector_from_obj(sel_obj);
	if (!sel) {
		pr_err("Bad Selector handle\n");
		return -EINVAL;
	}

	input_pad_class = get_class_from_class_type(SWITCH_OBJ_CLASS_INPUT_PAD);

	pad = kzalloc(sizeof(struct input_pad_s), GFP_KERNEL);
	if (!pad) {
		pr_err("Failed to allocate input pad for 0x%p selector\n", sel);
		return -ENOMEM;
	}

	snprintf(name, STM_REGISTRY_MAX_TAG_SIZE, "InputPad.0x%p",
			&pad->obj);

	/* Initialise embedded switch obj */
	err = switch_obj_init(&pad->obj, sel->obj.type, name, sel_obj,
			input_pad_class);
	if (err)
		goto error_obj_init;

	pad->connect_mode = STM_SELECTOR_PAD_CONNECT_REBASE_MODE;
	atomic_set(&pad->usr_connect_mode, STM_SELECTOR_PAD_CONNECT_REBASE_MODE);
	pad->stream_offset = -1;
	pad->caching_done = false;
	pad->pcr_invalid = false;
	pad->buf_limit = 2 * MIN_FREE_SPACE;
	INIT_LIST_HEAD(&pad->cache_head.lh);
	list_add_tail(&pad->obj.lh, &sel->in_pad_list);
	pad->obj.ops = &input_pad_ops;
	*pad_obj = &pad->obj;
	pr_info("%s:%d> Pad 0x%p created for Selector 0x%p\n",
			__func__, __LINE__, *pad_obj, sel_obj);

	return 0;

error_obj_init:
	pr_err("%s:%d> Pad creation failed(%d) for Selector 0x%p\n",
			__func__, __LINE__, err, sel_obj);
	kfree(pad);
	return err;
}

/*
 * \brief Deletes a Input PAD object
 *
 * \param obj PAD object to delete
 *
 * \retval 0 Success
 */
static int input_pad_delete(struct switch_obj *pad_obj)
{
	int err = 0;
	struct  input_pad_s *pad = get_input_pad_from_obj(pad_obj);

	if (pad->src_obj)
		err = -EBUSY;

	if (!err)
		err = switch_obj_deinit(pad_obj);
	if (err)
		return err;

	list_del(&pad_obj->lh);

	/* check if it has an active associoation, if yes then remove it from
	 * the list
	 * */
	if (pad->association) {
		if (mutex_lock_interruptible(&pad->association->lock) != 0) {
			err = -EINTR;
			goto skip_assoc_remove;
		}

		pr_debug("%s:%d> Pad:0x%p Del group node:0x%p from group:0x%p\n",
				__func__, __LINE__,
				pad, &pad->associate_lh, pad->association);
		list_del(&pad->associate_lh);
		if (list_empty(&pad->association->associated_input_pad_list))
			switch_association_delete(pad->association);
		else
			mutex_unlock(&pad->association->lock);
	}

skip_assoc_remove:
	pr_info("%s:%d> Pad 0x%p deleted for Selector 0x%p\n",
			__func__, __LINE__, pad_obj, pad_obj->parent);
	kfree(pad);

	return err;
}


static int  __set_connect_mode(struct input_pad_s *pad,
			int connect_mode)
{
	struct switch_obj *sel_obj = pad->obj.parent;
	struct switch_obj *cur_pad_obj = NULL;
	struct input_pad_s *cur_pad;
	struct switch_association_s *group = pad->association;
	struct list_head *in_pad_list = selector_get_in_pad_list(sel_obj);
	int ret = 0;


	/*no action required*/
	if (atomic_read(&pad->usr_connect_mode) == connect_mode) {
		goto exit;
	}

	/* currently pad is in pass through mode but
	 * needs to updated to rebase mode
	 * */
	if (connect_mode == STM_SELECTOR_PAD_CONNECT_REBASE_MODE) {
		/* in case of pass through to caching we need to set the state
		 * as inavlid
		 * */
		if (group)
			group->state = ASSOCIATION_CACHING_INVALID;

		atomic_set(&pad->usr_connect_mode, connect_mode);
		pad->exit = 0;
		goto exit;
	}

	/* currently pad is in rebase mode and needs to be updated to
	 * pass through mode.
	 * First check if there is a pad with pass through mode and
	 * reset it to rebase mode.
	 */

	/* Loop on all allocated selectors */
	list_for_each_entry(cur_pad_obj, in_pad_list, lh) {
		cur_pad = get_input_pad_from_obj(cur_pad_obj);

		/* locate the current input pad with pass through mode
		 * If found, reset it to rebase.
		 */
		if (cur_pad->connect_mode ==
			STM_SELECTOR_PAD_CONNECT_PASS_THROUGH_MODE) {
			atomic_set(&cur_pad->usr_connect_mode,
					STM_SELECTOR_PAD_CONNECT_REBASE_MODE);
			break;
		}
	}

	/*take lock on pad to be set in pass through mode
	   to ensure that it completes its rebasing logic if in progress
	*/
	/* currently pad is in rebase mode but
	 * needs to be updated to pass through mode*/
	atomic_set(&pad->usr_connect_mode, connect_mode);
	pad->exit =1;

exit:
	return ret;
}



static int __update_rebase_policy(struct input_pad_s *pad,
			int new_rebase_policy)
{
	int ret = 0;

	/*no action required*/
	if (pad->rebase_policy == new_rebase_policy) {
		goto exit;
	}

	/* if currently pad is in pass through mode then we can change the policy directly
	 * */
	if (pad->connect_mode == STM_SELECTOR_PAD_CONNECT_PASS_THROUGH_MODE) {
		pad->rebase_policy = new_rebase_policy;
		goto exit;
	}

	/*take lock on pad since its in rebase mode and ensure that
	   it completes its rebasing logic if in progress
	*/

	if (mutex_lock_interruptible(&pad->obj.lock) != 0)
		return -EINTR;

	/* currently pad is in  rebase mode but
	 * needs to be updated to pass through mode*/
	pad->rebase_policy = new_rebase_policy;

	mutex_unlock(&pad->obj.lock);

exit:
	return ret;
}

int input_pad_set_control(struct switch_obj *pad_obj, uint32_t control,
		const void *buf, uint32_t size)
{
	int ret = 0;
	int val = 0;
	struct input_pad_s *pad = get_input_pad_from_obj(pad_obj);


	if (!pad)
		return -EINVAL;

	switch (control) {
	case STM_SELECTOR_PAD_CONTROL_CONNECT_MODE:
		ret = SET_CONTROL(val, buf, size);
		if (ret)
			break;
		ret = __set_connect_mode(pad, val);
		if (!ret && val != STM_SELECTOR_PAD_CONNECT_REBASE_MODE) {
			if (mutex_lock_interruptible(&pad->obj.lock) != 0)
				return -EINTR;
			ret = __update_pad_connect_mode(pad, pad->obj.parent);
			__empty_cache_list(pad);
			mutex_unlock(&pad->obj.lock);
		}
		break;
	case STM_SELECTOR_PAD_CONTROL_REBASE_POLICY:
		ret = SET_CONTROL(val, buf, size);
		ret = __update_rebase_policy(pad, val);
		break;
	case STM_SELECTOR_PAD_CONTROL_CACHED_UNIT_COUNT:
		ret = SET_CONTROL(pad->cache_unit_cnt, buf, size);
		val = pad->cache_unit_cnt;
		break;
	case STM_SELECTOR_PAD_CONTROL_BUFFER_SIZE:
		ret = SET_CONTROL(val, buf, size);
		pad->buffer_size = val;
		break;
	default:
		pr_err("Invalid Set Control %d for pad:0x%p\n", control, pad);
		ret = -EINVAL;
		break;
	}

	if (!ret)
		pr_info("%s:%d> control %d set to value %d for pad 0x%p\n",
				__func__, __LINE__, control, val, pad);
	else
		pr_err("%d control failed to set to value %d for pad 0x%p\n",
				control, val, pad);

	return ret;
}

int input_pad_get_control(struct switch_obj *pad_obj, uint32_t control,
		void *buf, uint32_t size)
{
	int ret = 0;
	struct input_pad_s *pad = get_input_pad_from_obj(pad_obj);

	if (!pad)
		return -EINVAL;

	switch (control) {
	case STM_SELECTOR_PAD_CONTROL_CONNECT_MODE:
		ret = GET_CONTROL(pad->connect_mode, buf, size);
		break;
	case STM_SELECTOR_PAD_CONTROL_REBASE_POLICY:
		ret = GET_CONTROL(pad->rebase_policy, buf, size);
		break;
	case STM_SELECTOR_PAD_CONTROL_CACHED_UNIT_COUNT:
		ret = GET_CONTROL(pad->cache_unit_cnt, buf, size);
		break;
	case STM_SELECTOR_PAD_CONTROL_BUFFER_SIZE:
		ret = GET_CONTROL(pad->buffer_size, buf, size);
		break;
	default:
		pr_err("Invalid Get Control %d for pad:0x%p\n", control, pad);
		ret = -EINVAL;
		break;
	}

	return ret;
}


/*this function should not be called */
static int input_pad_attach(struct switch_obj *src_object,
		stm_object_h sink_object)
{
	return -EPERM;
}

/*this function should not be called */
static int input_pad_detach(struct switch_obj *src_object,
		stm_object_h sink_object)
{

	return -EPERM;
}

static int input_pad_data_push_attach(stm_object_h src_object,
		stm_object_h sink_object)
{
	int ret = 0;
	struct input_pad_s *pad = NULL;
	enum switch_obj_type_e obj_type;

	ret = get_in_pad_from_hdl(sink_object, &pad);

	if (!pad)
		return -EINVAL;

	if (mutex_lock_interruptible(&pad->obj.lock) != 0)
		return -EINTR;

	obj_type = get_obj_type(&pad->obj);

	if (SWITCH_OBJ_TYPE_AUDIO == obj_type ||
		SWITCH_OBJ_TYPE_VIDEO == obj_type) {
		pad->src_obj = src_object;
		pr_info("%s:%d> Pad 0x%p attached to source 0x%p\n",
			__func__, __LINE__, sink_object, src_object);
	} else {
		ret = -EPERM;
		pr_info("%s:%d> Pad 0x%p attachment failed with source 0x%p\n",
			__func__, __LINE__, sink_object, src_object);
	}
	mutex_unlock(&pad->obj.lock);

	return ret;
}

static int input_pad_data_push_detach(stm_object_h src_object,
		stm_object_h sink_object)
{
	int ret = 0;
	struct input_pad_s *pad = NULL;
	enum switch_obj_type_e obj_type;
	struct list_head *node_lh , *temp_lh;
	struct cache_node_s *cache_node;

	ret = get_in_pad_from_hdl(sink_object, &pad);

	if (!pad)
		return -EINVAL;

	if (mutex_lock_interruptible(&pad->obj.lock) != 0)
		return -EINTR;

	obj_type = get_obj_type(&pad->obj);
	switch (obj_type) {
	case SWITCH_OBJ_TYPE_AUDIO:
		pad->src_obj = NULL;
		break;
	case SWITCH_OBJ_TYPE_VIDEO:
		pad->src_obj = NULL;
		if (pad->association) {
			pad->association->gop_pts = INVALID_PTS;
			pad->association->state = ASSOCIATION_CACHING_INVALID;
		}
		break;
	default:
		ret = -EPERM;
	}

	/*If an active list for cached data then release it*/
	pr_info("%s:%d> detach sink 0x%p with pad 0x%p\n",
			__func__, __LINE__, src_object, pad);
	if (!list_empty(&pad->cache_head.lh)) {
		pr_debug("%s:%d> PAD:0x%p Cache List not empty\n",
			__func__, __LINE__, pad);

		list_for_each_safe(node_lh, temp_lh, &pad->cache_head.lh) {
			cache_node = list_entry(node_lh,
					struct cache_node_s, lh);
			pr_debug("%s:%d> PAD:0x%p Delete CacheNode:0x%p\n",
				__func__, __LINE__, pad, cache_node);

			list_del(&cache_node->lh);
			kfree(cache_node);

			if (list_empty(&pad->cache_head.lh))
				break;
		}
	}

	mutex_unlock(&pad->obj.lock);

	return ret;
}


static int input_pad_clk_push_attach(stm_object_h src_object,
		stm_object_h sink_object)
{
	int ret = 0;
	struct input_pad_s *pad = NULL;

	ret = get_in_pad_from_hdl(sink_object, &pad);

	if (!pad)
		return -EINVAL;

	if (mutex_lock_interruptible(&pad->obj.lock) != 0)
		return -EINTR;

	if (SWITCH_OBJ_TYPE_PCR == get_obj_type(&pad->obj)) {
		pad->src_obj = src_object;
		pr_info("%s:%d> Pad 0x%p attached to source 0x%p\n",
			__func__, __LINE__, sink_object, src_object);
	} else {
		ret = -EPERM;
		pr_info("%s:%d> Pad 0x%p attached to source 0x%p\n",
			__func__, __LINE__, sink_object, src_object);
	}
	mutex_unlock(&pad->obj.lock);

	return ret;
}


static int input_pad_clk_push_detach(stm_object_h src_object,
		stm_object_h sink_object)
{
	int ret = 0;
	struct input_pad_s *pad = NULL;

	ret = get_in_pad_from_hdl(sink_object, &pad);

	if (!pad)
		return -EINVAL;

	if (mutex_lock_interruptible(&pad->obj.lock) != 0)
		return -EINTR;

	if (SWITCH_OBJ_TYPE_PCR == get_obj_type(&pad->obj))
		pad->src_obj = NULL;
	else
		ret = -EPERM;

	mutex_unlock(&pad->obj.lock);

	return ret;
}

static int __send_pcr_to_associated_in_pads(struct input_pad_s *pcr_pad,
				unsigned long long pcr)
{
	struct input_pad_s *cur_pad;
	struct list_head *list_head;
	
	/* Check if PCR pad is associated with A/V pads
	 * */
	if (pcr_pad->association == NULL)
		goto exit;

	if (mutex_lock_interruptible(&pcr_pad->association->lock) != 0)
		return -EINTR;

	list_head = &pcr_pad->association->associated_input_pad_list;

	list_for_each_entry(cur_pad, list_head, associate_lh) {
		if (cur_pad != pcr_pad) {
			if (cur_pad->latest_pcr > 0 && InTimePeriod(pcr, cur_pad->latest_pcr,
					PcrLimit(cur_pad->latest_pcr + MAX_PCR_JUMP)) == 0) {
				pr_info("%s:%d> Inpad 0x%p invalid PCR jump %llu->%llu\n",
						__func__, __LINE__, cur_pad,
						cur_pad->latest_pcr, pcr);
				cur_pad->pcr_invalid = true;
			}
			cur_pad->latest_pcr = pcr;
			pr_debug("%s:%d> Association:0x%p PCR %llu updated for input pad 0x%p\n",
				__func__, __LINE__,
				pcr_pad->association, pcr, cur_pad);
		}
	}
	mutex_unlock(&pcr_pad->association->lock);

exit:
	return 0;
}

static int __update_pad_connect_mode(struct input_pad_s *pad,
		struct switch_obj *sel_obj)
{
	int connect_mode;
	int ret = 0;

	connect_mode = atomic_read(&pad->usr_connect_mode);
	if (connect_mode != pad->connect_mode) {
		/* connect mode changed since last push*/
		if (connect_mode == STM_SELECTOR_PAD_CONNECT_PASS_THROUGH_MODE) {
			ret = mutex_lock_interruptible(&pad->association->lock);
			if (ret) {
				pr_err("%s:%d> Interrupted while setting pass through mode\n",
						__func__, __LINE__);
				return ret;
			}
			if (pad->association->state != ASSOCIATION_CACHING_DONE) {
				mutex_unlock(&pad->association->lock);
				pr_debug("%s:%d> Inpad 0x%p unable to pass through\n",
						__func__, __LINE__, pad);
				return -EBUSY;
			}
			pr_debug("%s:%d> Inpad 0x%p pass through request success\n",
						__func__, __LINE__, pad);
			mutex_unlock(&pad->association->lock);

			if (pad->is_secure == true)
				ret = selector_set_secure_path_status(sel_obj,
						true);
		} else if (connect_mode == STM_SELECTOR_PAD_CONNECT_FORCE_PASS_THROUGH_MODE) {
			pr_debug("%s:%d> Inpad 0x%p force pass through request\n",
					__func__, __LINE__, pad);
			connect_mode = STM_SELECTOR_PAD_CONNECT_PASS_THROUGH_MODE;
			atomic_set(&pad->usr_connect_mode, STM_SELECTOR_PAD_CONNECT_PASS_THROUGH_MODE);
			if (pad->is_secure == true)
				ret = selector_set_secure_path_status(sel_obj,
						true);
		}
		pad->connect_mode = connect_mode;
	}
	return ret;
}


static int input_pad_set_clock_data_point(stm_object_h pad_h,
				stm_se_time_format_t time_format,
				bool new_seq,
				unsigned long long src_time,
				unsigned long long sys_time)
{
	int ret = 0;

	struct input_pad_s  *input_pad = NULL;
	struct switch_obj *sel_obj = NULL;
	struct switch_association_s *group;

	get_in_pad_from_hdl(pad_h, &input_pad);
	if (!input_pad) {
		pr_err("Invalid handle\n");
		return -EINVAL;
	}

	if (mutex_lock_interruptible(&input_pad->obj.lock) != 0)
		return -EINTR;

	sel_obj = (input_pad->obj.parent);
	group = input_pad->association;

	ret = __update_pad_connect_mode(input_pad, sel_obj);
	if (ret)
		pr_err("Updation for Connect Mode failed(%d)\n", ret);

	input_pad->time_info.time_format = time_format;
	input_pad->time_info.source_time = src_time;
	input_pad->time_info.system_time = sys_time;

	/* Check if this Input in pass through mode i.e.
	 * needs to be directly sent to  sink*/
	if (input_pad->connect_mode ==
			STM_SELECTOR_PAD_CONNECT_PASS_THROUGH_MODE) {
		ret = selector_set_clock_data_point(sel_obj,
				time_format, new_seq,
				src_time, sys_time);
		pr_debug("%s:%d> Pushing from SR:0x%p to IP:0x%p\n",
				__func__, __LINE__,
				input_pad, input_pad->src_obj);
	} else {
		/* send the pcr info to associated input pads */
		ret = __send_pcr_to_associated_in_pads(input_pad, src_time);
	}

	mutex_unlock(&input_pad->obj.lock);

	return ret;
}

static int input_pad_push_data(stm_object_h pad_h,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks)
{
	int ret = 0;

	struct input_pad_s  *input_pad = NULL;
	struct switch_obj *sel_obj = NULL;
	struct switch_association_s *group;
	int rb_len = 0;
	uint32_t rebased_bytes;
	enum switch_obj_type_e obj_type;

	get_in_pad_from_hdl(pad_h, &input_pad);

	if (!input_pad) {
		pr_err("Invalid handle\n");
		return -EINVAL;
	}
	obj_type = get_obj_type(&input_pad->obj);

	if (mutex_lock_interruptible(&input_pad->obj.lock) != 0)
		return -EINTR;

	group = input_pad->association;
	sel_obj = (input_pad->obj.parent);
	ret = __update_pad_connect_mode(input_pad, sel_obj);
	if (ret)
		pr_err("Updation for Connect Mode failed(%d)\n", ret);

	/* Check if this Input in pass through mode i.e.
	 * needs to be directly sent to  sink*/
	if (input_pad->connect_mode ==
			STM_SELECTOR_PAD_CONNECT_PASS_THROUGH_MODE) {
		if (group) {
			pr_debug("%s:%d> Pushing from SRC:0x%p to IP:0x%p\n",
					__func__, __LINE__,
					input_pad->src_obj, input_pad);
			ret = selector_push_data(sel_obj, block_list,
						block_count, data_blocks);
		}
	} else {
		/* process data for caching */
		pr_debug("%s:%d> Caching data from SRC:0x%p to IP:0x%p IPState:%d\n",
				__func__, __LINE__,
				input_pad->src_obj, input_pad, group->state);
		ret = __cache_data(input_pad, block_list,
				block_count, data_blocks);
		if (SWITCH_OBJ_TYPE_VIDEO == obj_type) {
			rb_len = block_list[0].len;
			if (block_count > 1)
				rb_len += block_list[1].len;
			rb_len -= *data_blocks;
			pr_debug("%s:%d> Inpad 0x%p rb_len %d buf size %d\n",
					__func__, __LINE__, input_pad,
					rb_len, input_pad->buffer_size);
			while (rb_len > (input_pad->buffer_size - input_pad->buf_limit)) {
				__rebase_single_gop(input_pad, &rebased_bytes);
				pr_info("%s:%d:> Inpad 0x%p potential buf overflow. Rebased %d bytes\n",
						__func__, __LINE__, input_pad,
						rebased_bytes);
				rb_len -= rebased_bytes;
				*data_blocks += rebased_bytes;
			}
			__update_caching_status(input_pad);
		}
	}

	mutex_unlock(&input_pad->obj.lock);

	return ret;
}

static int input_pad_set_secure_path_status(stm_object_h pad_h,
					bool secure_flag)
{
	int ret = 0;

	struct input_pad_s  *input_pad = NULL;
	struct switch_obj *sel_obj = NULL;

	get_in_pad_from_hdl(pad_h, &input_pad);
	if (!input_pad) {
		pr_err("Invalid handle\n");
		return -EINVAL;
	}

	if (mutex_lock_interruptible(&input_pad->obj.lock) != 0)
		return -EINTR;

	sel_obj = (input_pad->obj.parent);

	input_pad->is_secure = secure_flag;
	/* Check if this Input in pass through mode i.e.
	 * needs to be directly sent to  sink*/
	if (input_pad->connect_mode ==
		STM_SELECTOR_PAD_CONNECT_PASS_THROUGH_MODE)
		ret = selector_set_secure_path_status(sel_obj, secure_flag);

	mutex_unlock(&input_pad->obj.lock);

	return ret;
}


int input_pad_add_to_association(struct input_pad_s *in_pad,
			struct switch_association_s *association)
{
	if (!in_pad || !association) {
		pr_err("Invalid Pad: 0x%p or Association Handle: 0x%p\n",
				in_pad, association);
		return -EINVAL;
	}

	if (mutex_lock_interruptible(&association->lock) != 0)
		return -EINTR;

	list_add_tail(&in_pad->associate_lh,
			&association->associated_input_pad_list);
	in_pad->association = association;
	pr_debug("%s:%d> Pad:0x%p Add GroupNode:0x%p to Group:0x%p\n",
			__func__, __LINE__,
			in_pad, &in_pad->associate_lh, association);
	mutex_unlock(&association->lock);

	return 0;
}

int input_pad_set_start_code(struct input_pad_s *input_pad,
		const char *start_code, unsigned int len)
{
	int ret = 0;
	int i;
	/* we should lock this op becuase we should not
	 * overrride the start code while an active push
	*/

	if (len <= PAD_PES_START_CODE_ARR_SZ) {
		memcpy(input_pad->start_code, start_code, len);
		input_pad->num_sc = len;
		for (i = 0; i < len; i++) {
			pr_debug("%s:%d> Inpad 0x%p SC 0x%x\n", __func__,
					__LINE__, input_pad, start_code[i]);
		}

		ret = pes_mp2_get_sc_map(input_pad->start_code,
				input_pad->num_sc,
				input_pad->sc_map, PAD_PES_START_CODE_MAP_SZ);

	}
	else
		ret = -EINVAL;
	return ret;
}


static inline int __reject_this_push(struct stm_data_block *block_list,
			uint32_t block_count, uint32_t *data_blocks)
{
	uint32_t i = 0;

	*data_blocks = 0;

	for (; i < block_count; i++) {
		pr_debug("%s:%d> Rejecting %d Bytes StartPos: 0x%p\n",
				__func__, __LINE__,
				block_list[i].len, block_list[i].data_addr);
		*data_blocks += block_list[i].len;
	}
	pr_debug("%s:%d> reject %d bytes\n", __func__, __LINE__, *data_blocks);

	return 0;
}




static inline int __cp_pes_sync_bytes(struct input_pad_s *pad,
				uint8_t *pes_holder)
{
	int blocks = 0;
	int total = 0;
	int bytes = 0;
	uint32_t blk_cnt = pad->block_count;

	for (; blocks < blk_cnt; blocks++) {
		bytes = min_t(uint32_t, pad->block[blocks].len, 3);
		memcpy((void *)(&pes_holder[total]),
				(void *)(pad->block[blocks].data_addr),
				bytes);
		total += bytes;
		if (total >= 3)
			break;
	}
	/* check start code and stream_id */
	pr_debug("%s:%d> Pad:0x%p sync Bytes: 0x%02x 0x%02x 0x%02x\n",
			__func__, __LINE__, pad,
			pes_holder[0], pes_holder[1], pes_holder[2]);

	return total;
}

static void __cp_src_buf_info(struct input_pad_s *in_pad,
			struct stm_data_block *pad_blk,
			struct stm_data_block *src_blk,
			uint32_t offset)
{
	pad_blk->data_addr = (uint8_t *)(src_blk->data_addr) + offset;
	pad_blk->len = src_blk->len - offset;
	in_pad->block_count++;
	pr_debug("%s:%d> start: 0x%p len: %d for pad: 0x%p\n",
			__func__, __LINE__,
			pad_blk->data_addr,
			pad_blk->len,
			in_pad);

}


static int __locate_pes_start(struct input_pad_s *in_pad,
			struct stm_data_block *block_list,
			uint32_t block_count)
{
	struct cache_node_s *cache_node = NULL;
	uint32_t review_buffer_len = 0;

	list_for_each_entry(cache_node, &in_pad->cache_head.lh, lh)
		review_buffer_len += cache_node->cache_len;

	pr_debug("%s:%d> review buf len: %d for pad: 0x%p\n",
			__func__, __LINE__, review_buffer_len, in_pad);

	/* hard code the block list logic i.e.
	 * it is presumed that we will have max 2 block list*/
	in_pad->block_count = 0;
	if (block_list[0].len >= review_buffer_len) {
		__cp_src_buf_info(in_pad, &in_pad->block[0],
				&block_list[0], review_buffer_len);
		if (block_count > 1)
			__cp_src_buf_info(in_pad, &in_pad->block[1],
					&block_list[1], 0);
	} else {
		review_buffer_len -= block_list[0].len;
		__cp_src_buf_info(in_pad, &in_pad->block[0],
				&block_list[1], review_buffer_len);
	}
	pr_debug("%s:%d> Pad: 0x%p TotalBlkCount:%d\n",
			__func__, __LINE__, in_pad, in_pad->block_count);

	return 0;
}

static inline struct cache_node_s *__alloc_cache_node(struct input_pad_s *in_pad)
{
	struct cache_node_s *cache_node;
	struct timespec ts;
	ktime_t	ktime;

	cache_node = kzalloc(sizeof(struct cache_node_s), GFP_KERNEL);
	if (!cache_node) {
		pr_err("GOP node allocation failed\n");
		return NULL;
	}
	
	pr_debug("%s:%d> new node 0x%p added to head 0x%p for pad 0x%p\n",
			__func__, __LINE__,
			cache_node, &in_pad->cache_head.lh, in_pad);
	
	in_pad->cache_list_cnt++;

	getrawmonotonic(&ts);
	ktime = timespec_to_ktime(ts);
	cache_node->ktime = ktime_to_us(ktime);

	list_add_tail(&cache_node->lh, &in_pad->cache_head.lh);
	return cache_node;
}

static inline int __dealloc_cache_node(struct input_pad_s *in_pad,
		struct cache_node_s *node)
{
	pr_debug("%s:%d> CacheNode 0x%p deleted for pad 0x%p\n",
			__func__, __LINE__,
			node, in_pad);
	
	in_pad->cache_list_cnt--;

	list_del(&node->lh);
	kfree(node);
	return 0;
}


static int __parse_pes_header(struct input_pad_s *in_pad,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks)
{
	int ret = 0;
	uint8_t pes_holder[3] = {0};

	__locate_pes_start(in_pad, block_list, block_count);

	/*Check if first block list has just 2 bytes*/
	ret = __cp_pes_sync_bytes(in_pad, pes_holder);
	if (ret < 3) {
		/*we do not have enough data to validate the pes
		* so lets wait for more data
		*/
		*data_blocks = 0;
		return -ENOMEM;
	}

	ret = pes_mp2_validate_header(pes_holder, 3);
	/*PES not found , so reject this push*/

	return 0;
}


static int __process_pes_for_caching(struct input_pad_s *in_pad,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks)
{
	int ret = 0;
	struct pad_pes_pkt_s *pkt;
	/*struct cache_node_s *cache_node;*/
	struct cache_node_s *latest_cache_node;

	/* we need to calculate the expected loation of the PES in buffer.
	 * If this is a first push for caching then it is expected that
	 * start pointer from source will point to PES. Else we will calculate
	 * the PES start based on the last PES size in the GOP info list
	*/
	ret = __parse_pes_header(in_pad, block_list, block_count, data_blocks);
	if (ret) {
		pr_info("%s:%d> Invalid PES Header: Pad:0x%p\n",
				__func__, __LINE__, in_pad);
		goto exit;
	}
	/*We have a valid PES now, lets parse it*/
	/*
	cache_node = list_first_entry(&in_pad->cache_head.lh,
			struct cache_node_s, lh);
	*/
	latest_cache_node = list_prev_entry(&in_pad->cache_head, lh);
	pr_debug("%s:%d> TailCacheNode: 0x%p for pad: 0x%p\n",
			__func__, __LINE__,
			latest_cache_node, in_pad);

	/*Reset pes pkt info*/
	pkt = &latest_cache_node->pkt;
	memset(pkt, 0, sizeof(struct pad_pes_pkt_s));	
	pkt->cur_pos = (uint8_t *)in_pad->block[0].data_addr;
	pr_debug("%s:%d> CurPkt:0x%p Len:%d LatestNode:0x%p Pad:0x%p c %d\n",
			__func__, __LINE__,
			 pkt, in_pad->block[0].len,
			 latest_cache_node, in_pad, block_count);

	pkt->status = PAD_PES_PKT_STATUS_IN_PROGRESS;
	ret = pes_mp2_parse_pkt(pkt, in_pad->block[0].len,
				in_pad->sc_map,
				in_pad->num_sc, &in_pad->exit);

	/* check if more data to be sent to complete parsing
	 * and we have data to be sent.
	 * If we have more data and no sc is found yet, we need to look till
	 * the end.
	 * */
	if (in_pad->block_count > 1 && pkt->has_sc != true) {
		pkt->cur_pos = (uint8_t *)in_pad->block[1].data_addr;
		pr_debug("%s:%d> CurPkt:0x%p Len:%d LatestNode:0x%p Pad:0x%p\n",
			__func__, __LINE__,
			 pkt, in_pad->block[1].len,
			 latest_cache_node, in_pad);

		ret = pes_mp2_parse_pkt(pkt, in_pad->block[1].len,
					in_pad->sc_map,
					in_pad->num_sc,
					&in_pad->exit);
	}

	/*check the pes data status*/
	if (pkt->status != PAD_PES_PKT_STATUS_OK) {
		/*reject data*/
		pr_err("FATAL: Pad:0x%p invalid PES detected, reject all cached data\n",
				in_pad);
		ret = -EINVAL;
	} else {
		pr_debug("%s:%d> Pad:0x%p Pkt:0x%p ESCount:%d Good PES detected\n",
				__func__, __LINE__, in_pad, pkt, pkt->es_cnt);
	}

exit:
	return ret;
}

static int __empty_cache_list(struct input_pad_s *in_pad)
{
	struct list_head *node_lh , *temp_lh;
	struct cache_node_s *cache_node;

	list_for_each_safe(node_lh, temp_lh, &in_pad->cache_head.lh) {
		cache_node = list_entry(node_lh,
					struct cache_node_s, lh);
		pr_debug("%s:%d> Rejecting %d Bytes CacheNode: 0x%p\n",
				__func__, __LINE__,
				cache_node->cache_len, cache_node);
		__dealloc_cache_node(in_pad, cache_node);
		if (list_empty(&in_pad->cache_head.lh))
			break;
	}

	return 0;
}

static int __reject_all_cached_data(struct input_pad_s *in_pad,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks)
{
	__reject_this_push(block_list, block_count, data_blocks);

	if (list_empty(&in_pad->cache_head.lh))
		return 0;

	__empty_cache_list(in_pad);

	return 0;
}

static int __is_push_duplicate(struct input_pad_s *in_pad,
		struct stm_data_block *blk_list, uint32_t blk_cnt)
{
	uint32_t review_buf_len = 0, src_buf_len = 0, blk =0;
	int ret = 0;
	struct cache_node_s *cache_node = NULL;

	list_for_each_entry(cache_node, &in_pad->cache_head.lh, lh)
		review_buf_len += cache_node->cache_len;

	for (; blk < blk_cnt; blk++)
		src_buf_len += blk_list[blk].len;

	if (src_buf_len == review_buf_len) {
		pr_debug("%s:%d> Duplicate push: PAD:0x%p ReviewLen:%d SrcLen:%d\n",
				__func__, __LINE__, in_pad,
				review_buf_len, src_buf_len);
		ret = 1;
	} else if (src_buf_len > review_buf_len) {
		pr_debug("%s:%d> Valid Push: PAD:0x%p ReviewLen:%d SrcLen:%d\n",
				__func__, __LINE__, in_pad,
				review_buf_len, src_buf_len);
		ret = 0;
	} else {
		pr_debug("%s:%d> Valid Push: PAD:0x%p ReviewLen:%d SrcLen:%d\n",
				__func__, __LINE__, in_pad,
				review_buf_len, src_buf_len);
		ret = -EILSEQ;
	}	

	return ret;
}

static void __update_gop_pts(struct input_pad_s *in_pad)
{
	struct cache_node_s *cache_node;

	if (in_pad->association) {
		if (mutex_lock_interruptible(&in_pad->association->lock)) {
			pr_err("%s:%d> Interrupted while updating GOP PTS\n",
					__func__, __LINE__);
			return;
		}
		cache_node = list_first_entry(&in_pad->cache_head.lh,
			struct cache_node_s, lh);
		if (cache_node == &in_pad->cache_head) {
			/* Caching list empty */
			in_pad->association->gop_pts = INVALID_PTS;
			in_pad->association->oldest_vid_pts = INVALID_PTS;
			in_pad->association->gop_time = 0;
		} else {
			in_pad->association->gop_pts = cache_node->pts;
			in_pad->association->oldest_vid_pts = cache_node->pts;
			in_pad->association->gop_time = cache_node->ktime;
		}
		mutex_unlock(&in_pad->association->lock);
	}

	return;
}

static void __rebase_single_gop(struct input_pad_s *in_pad, uint32_t *bytes)
{
	struct cache_node_s *cache_node;

	cache_node = list_first_entry(&in_pad->cache_head.lh,
			struct cache_node_s, lh);

	*bytes = cache_node->cache_len;
	__dealloc_cache_node(in_pad, cache_node);
	__update_gop_pts(in_pad);

	return;
}

static void __update_caching_status(struct input_pad_s *in_pad)
{
	if (!in_pad->association) {
		pr_err("%s:%d> Input pad 0x%p has no associations\n",
				__func__, __LINE__, in_pad);
		return;
	}

	if (mutex_lock_interruptible(&in_pad->association->lock)) {
		pr_err("%s:%d> Interrupted while updating GOP PTS\n",
				__func__, __LINE__);
		return;
	}

	if (in_pad->caching_done) {
		if (in_pad->association->gop_pts == INVALID_PTS ||
			TimeIsBefore(in_pad->latest_pcr, in_pad->association->gop_pts)) {
			in_pad->caching_done = false;
			in_pad->association->state = ASSOCIATION_GOP_ACQUIRED;
			pr_debug("Inpad 0x%p min caching failed GOP PTS %llu PCR %llu\n",
				in_pad, in_pad->association->gop_pts,
				in_pad->latest_pcr);
		}
	} else {
		if (in_pad->association->gop_pts != INVALID_PTS &&
			(TimeIsBefore(in_pad->association->gop_pts, in_pad->latest_pcr) ||
				in_pad->association->gop_pts == in_pad->latest_pcr)) {
			in_pad->caching_done = true;
			in_pad->association->state = ASSOCIATION_CACHING_DONE;
			pr_debug("Inpad 0x%p min caching done GOP PTS %llu PCR %llu\n",
				in_pad, in_pad->association->gop_pts,
				in_pad->latest_pcr);
		}
	}

	if (in_pad->association->gop_pts == INVALID_PTS)
		in_pad->association->state = ASSOCIATION_CACHING_INVALID;

	mutex_unlock(&in_pad->association->lock);

	return;
}

static int __cache_video_data(struct input_pad_s *in_pad,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks)
{
	int ret = 0;
	struct pad_pes_pkt_s *pkt;
	struct cache_node_s *cache_node, *next_cache_node, *latest_cache_node;
	struct switch_association_s *group = in_pad->association;

	/*check the GOP status*/
	cache_node = list_first_entry(&in_pad->cache_head.lh,
			struct cache_node_s, lh);
	latest_cache_node = list_prev_entry(&in_pad->cache_head, lh);
	
	pkt = &latest_cache_node->pkt;

	if (in_pad->pcr_invalid && group) {
		__reject_all_cached_data(in_pad, block_list,
				block_count, data_blocks);
		__update_gop_pts(in_pad);
		in_pad->pcr_invalid = false;
		goto exit;
	}
	/* If this is the first caching attempt and this packet dosent have
	 * GOP data then reject it
	 * */
	if (latest_cache_node->cache_len == 0 &&
			pkt->has_sc == false) {
		pr_debug("%s:%d> First GOP pending PAD:0x%p Node:0x%p PTS:%llu\n"
				, __func__, __LINE__,
				in_pad, cache_node, pkt->pts);
		if (group && pkt->has_pts) {
			in_pad->stream_offset = pkt->pts - in_pad->latest_pcr;
			group->oldest_vid_pts = pkt->pts;
			group->state = ASSOCIATION_FIRST_VID_VALID_PTS;
		}

		__reject_all_cached_data(in_pad, block_list,
				block_count, data_blocks);
		goto exit;
	}

	if (pkt->has_sc == true) {
		if (group && (group->state == ASSOCIATION_CACHING_INVALID ||
			group->state == ASSOCIATION_FIRST_VID_VALID_PTS))
			in_pad->association->state = ASSOCIATION_GOP_ACQUIRED;

		/* If there are more than 1 node in the list*/
		if (latest_cache_node->sc_present == true)
			latest_cache_node = __alloc_cache_node(in_pad);

		if (pkt->has_pts) {
			latest_cache_node->pts = pkt->pts;
			in_pad->stream_offset = pkt->pts - in_pad->latest_pcr;
		} else if (in_pad->stream_offset > 0)
			/*calculate approximate PTS for this PES*/
			latest_cache_node->pts =
				in_pad->stream_offset + in_pad->latest_pcr;

		latest_cache_node->sc_present = true;
	}

	pr_debug("%s:%d> Input pad 0x%p has_sc %d state %d\n", __func__,
			__LINE__, in_pad, pkt->has_sc,
			in_pad->association->state);

	latest_cache_node->cache_len += in_pad->block[0].len;
	if (in_pad->block_count > 1)
		 latest_cache_node->cache_len += in_pad->block[1].len;

	pr_debug("%s:%d> Cached %d VideoBytes PAD:0x%p CurNode:0x%p Pkt:0x%p Offset:%llu\n",
				__func__, __LINE__,
				latest_cache_node->cache_len, in_pad,
				latest_cache_node, pkt, in_pad->stream_offset);


	/*check if we need to rebase to next GOP */
	cache_node = list_first_entry(&in_pad->cache_head.lh,
			struct cache_node_s, lh);
	next_cache_node = list_next_entry(cache_node, lh);

	/* if next GOP's PTS is < pcr then we have buffered data
	 * equal to stream offset so shift to next GOP
	*/
	pr_debug("%s:%d> PAD:0x%p PTS:%llu NextPTS:%llu CurPCR:%llu CacheCnt:%d\n",
				__func__, __LINE__, in_pad, cache_node->pts,
				next_cache_node->pts, in_pad->latest_pcr,
				in_pad->cache_list_cnt);

	/* If only one node in the list, dont rebase even if its stale
	 * If more than 3 GOPS, then also do rebase. This is to ensure that
	 * in case we dont get PTS.
	 * */

	if (&next_cache_node->lh != &in_pad->cache_head.lh &&
			TimeIsBefore(next_cache_node->pts, in_pad->latest_pcr)) {
		pr_debug("%s:%d> PAD:0x%p Remove Stale HeadNode:0x%p Flush %d bytes\n",
				__func__, __LINE__,
				in_pad, cache_node, cache_node->cache_len);

		*data_blocks = cache_node->cache_len;
		__dealloc_cache_node(in_pad, cache_node);
	}

	__update_gop_pts(in_pad);

	return 0;
exit:
	return ret;
}


static int __cache_audio_data(struct input_pad_s *in_pad,
		struct stm_data_block *block_list,
		uint32_t block_count, uint32_t *data_blocks)
{
	struct pad_pes_pkt_s *pkt;
	struct cache_node_s *cache_node, *latest_cache_node, *next_cache_node;
	uint64_t time_stamp;
	struct list_head *node_lh , *temp_lh;
	struct switch_association_s *group = in_pad->association;
	latest_cache_node = list_prev_entry(&in_pad->cache_head, lh);
	pkt = &latest_cache_node->pkt;

	if (in_pad->pcr_invalid && group) {
		__reject_all_cached_data(in_pad, block_list,
				block_count, data_blocks);
		in_pad->pcr_invalid = false;
		goto exit;
	}
	/* If this is the first caching attempt and this packet dosent have
	 * PTS then reject it as we need atleast one PTS to start rebasing
	 * */
	if (latest_cache_node->cache_len == 0 &&
			pkt->has_pts == false) {
		pr_debug("%s:%d> First PTS PES pending PAD:0x%p Node:0x%p Pkt:0x%p\n",
				__func__, __LINE__, in_pad,
				latest_cache_node, pkt);

		__reject_all_cached_data(in_pad, block_list,
				block_count, data_blocks);
		goto exit;
	}

	if (pkt->has_pts) {
		/* If there is a node with PTS already then alloc a new node*/
		if (latest_cache_node->pts > 0)
			latest_cache_node = __alloc_cache_node(in_pad);

		latest_cache_node->pts = pkt->pts;
	}

	latest_cache_node->cache_len += in_pad->block[0].len;
	if (in_pad->block_count > 1)
		latest_cache_node->cache_len += in_pad->block[1].len;

	pr_debug("%s:%d> Cached %d AudioBytes PAD:0x%p CurNode:0x%p PTS:%llu\n",
				__func__, __LINE__,
				latest_cache_node->cache_len, in_pad,
				latest_cache_node, pkt->pts);

	/*is this pad associated?*/
	if (group) {
		if (group->gop_pts == INVALID_PTS) {
			time_stamp = in_pad->latest_pcr;
			pr_debug("%s:%d> PAD:0x%p Selected PCR as reference for Caching\n",
					__func__, __LINE__, in_pad);
		}
		else {
			time_stamp = group->gop_pts;
			pr_debug("%s:%d> PAD:0x%p Selected GOP PTS as reference for Caching\n",
					__func__, __LINE__, in_pad);
		}

		list_for_each_safe(node_lh, temp_lh, &in_pad->cache_head.lh) {
			cache_node = list_entry(node_lh,
					struct cache_node_s, lh);
			next_cache_node = list_next_entry(cache_node, lh);
			pr_debug("%s:%d> PAD:0x%p HeadNode:0x%p NodePTS:%llu GOP PTS:%llu PCR:%llu\n",
					__func__, __LINE__,
					in_pad, cache_node,
					cache_node->pts,
					group->gop_pts,
					in_pad->latest_pcr);

			/* if oldest PES PTS is < GOP PTS
			 * then we shift to next PES
			 * */
			if (next_cache_node && &next_cache_node->lh != &in_pad->cache_head.lh) {
				if (TimeIsBefore(next_cache_node->pts, time_stamp)) {
					pr_debug("%s:%d> PAD:0x%p Remove Stale HeadNode:0x%p Flush %d bytes\n",
							__func__, __LINE__,
							in_pad, cache_node,
							cache_node->cache_len);
					pr_debug("%s:%d> PAD:0x%p HeadPTS:%llu NxtPTS:%llu\n",
							__func__, __LINE__,
							in_pad, cache_node->pts,
							next_cache_node->pts);
					*data_blocks += cache_node->cache_len;
					__dealloc_cache_node(in_pad, cache_node);
				} else {
					break;
				}
			}
		}
	}
exit:
	return 0;
}


static int __cache_data(struct input_pad_s *in_pad,
		struct stm_data_block *block_list,
		uint32_t block_count, uint32_t *data_blocks)
{
	int ret = -EINVAL;
	enum switch_obj_type_e obj_type;

	obj_type = get_obj_type(&in_pad->obj);

	*data_blocks = 0;

	if (SWITCH_OBJ_TYPE_AUDIO != obj_type &&
			SWITCH_OBJ_TYPE_VIDEO != obj_type) {
		pr_err("Invalid obj(0x%p) type(%d)\n", in_pad, obj_type);
		goto exit;
	}

	if (list_empty(&in_pad->cache_head.lh)) {
		__alloc_cache_node(in_pad);
	}
	
	ret = __is_push_duplicate(in_pad, block_list, block_count);
	if (ret) {
		/* this is duplicate push so ignore it*/
		in_pad->buf_limit += MIN_FREE_SPACE;
		ret = 0;
		goto exit;
	}

	in_pad->buf_limit = 2 * MIN_FREE_SPACE;
	ret = __process_pes_for_caching(in_pad, block_list,
			block_count, data_blocks);

	if (in_pad->exit) {
		in_pad->exit = 0;
		goto exit;
	}
	/*check the pes data status*/
	if (ret) {
		__reject_all_cached_data(in_pad, block_list,
			block_count, data_blocks);
		goto exit;
	}

	if (SWITCH_OBJ_TYPE_AUDIO == obj_type)
		ret = __cache_audio_data(in_pad, block_list,
					block_count, data_blocks);
	else if (SWITCH_OBJ_TYPE_VIDEO == obj_type)
		ret = __cache_video_data(in_pad, block_list,
					block_count, data_blocks);
	else
		pr_err("Invalid obj(0x%p) type(%d)\n", in_pad, obj_type);
exit:
	return ret;
}

/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/**
 *  @File   switch_globall.h
 *  @brief
 *
 *
 *
 * */

#include "stm_selector.h"
#include "switch_obj.h"

#ifndef _SWITCH_GLOBAL_H
#define _SWITCH_GLOBAL_H

#define INVALID_PCR 0xFFFFFFFFFFFF
#define INVALID_PTS 0xFFFFFFFFFFFF

enum switch_association_state_e {
	ASSOCIATION_CACHING_INVALID,
	ASSOCIATION_FIRST_VID_VALID_PTS,
	ASSOCIATION_GOP_ACQUIRED,
	ASSOCIATION_CACHING_DONE
};

struct switch_association_s {
	char name[STM_REGISTRY_MAX_TAG_SIZE];
	enum switch_association_state_e state;
	struct list_head associated_input_pad_list;
	unsigned long long latest_pcr;
	unsigned long long gop_pts;
	unsigned long long oldest_vid_pts;
	unsigned long long gop_time;
	struct list_head lh;
	struct mutex lock;
};

struct switch_global {
	struct switch_obj_class selector_class;
	struct switch_obj_class input_pad_class;
	struct switch_obj_class output_pad_class;
	struct list_head sel_list;
	struct list_head associate_list;

	struct mutex lock;
};

struct switch_obj_class *get_class_from_class_type(enum switch_obj_class_e class_type);
int switch_global_get_sel_list(struct list_head **sel_list);
struct switch_association_s *switch_get_association_handle(const char *name);
int take_global_lock(void);
void release_global_lock(void);
int switch_association_new(const char *name,
		struct switch_association_s **association_h);
int switch_association_delete(struct switch_association_s *association);


#endif

/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include <stm_registry.h>
#include <linux/version.h>
#include <linux/module.h>
#include "switch_obj.h"
#include "selector.h"
#include "input_pad.h"
#include <switch_global.h>
#include <linux/atomic.h>

static int __unregister_classes(struct switch_global *switch_p);
static int __register_classes(struct switch_global *switch_p);

static struct switch_global g_switch;

static int __register_classes(struct switch_global *switch_p)
{
	int err;

	/* Add switch class */
	err = selector_register_class(&switch_p->selector_class);
	if (err)
		return err;

	/* Add input pad class */
	err = input_pad_register_class(&switch_p->input_pad_class);
	if (err)
		goto error_input_add_class;

	return 0;

error_input_add_class:
	selector_unregister_class(&switch_p->selector_class);
	return err;
}

static int __unregister_classes(struct switch_global *switch_p)
{
	int err;

	/* Add switch class */
	err = selector_unregister_class(&switch_p->selector_class);
	if (err)
		pr_err("selector_unregister_class failed(%d)", err);

	/* Add input pad class */
	err = input_pad_unregister_class(&switch_p->input_pad_class);
	if (err)
		pr_err("input_pad_unregister_class failed(%d)", err);

	return err;
}



struct switch_obj_class *get_class_from_class_type(enum switch_obj_class_e class_type)
{
	struct switch_global *switch_p = &g_switch;
	struct switch_obj_class *obj_class = NULL;

	switch (class_type) {
	case SWITCH_OBJ_CLASS_SELECTOR:
		obj_class = &switch_p->selector_class;
		break;
	case SWITCH_OBJ_CLASS_INPUT_PAD:
		obj_class = &switch_p->input_pad_class;
		break;
	case SWITCH_OBJ_CLASS_OUTPUT_PAD:
		obj_class = &switch_p->output_pad_class;
		break;
	default:
		pr_err("Invalid class Type:%d\n", class_type);
	}

	return obj_class;
}

int take_global_lock(void)
{
	struct switch_global *switch_p = &g_switch;

	if (mutex_lock_interruptible(&switch_p->lock) != 0)
		return -EINTR;

	return 0;
}

void release_global_lock(void)
{
	struct switch_global *switch_p = &g_switch;

	mutex_unlock(&switch_p->lock);
}

int switch_global_get_sel_list(struct list_head **sel_list)
{
	struct switch_global *switch_p = &g_switch;

	*sel_list = &switch_p->sel_list;

	return 0;
}

int switch_association_new(const char *name,
		struct switch_association_s **association_h)
{
	int ret = 0;
	struct switch_association_s *association_p;
	struct switch_global *switch_p = &g_switch;

	/* Sanity check name. The registry will do this for us later anyway.
	 * This check is so that we can comply with our documented return code
	 * of -ENAMETOOLONG */
	if (strnlen(name, STM_REGISTRY_MAX_TAG_SIZE) >
			STM_REGISTRY_MAX_TAG_SIZE - 1)
		return -ENAMETOOLONG;

	association_p = kzalloc(sizeof(struct switch_association_s),
				GFP_KERNEL);
	if (!association_p) {
		pr_err("No memory for new a new association\n");
		return -ENOMEM;
	}
	association_p->gop_pts = INVALID_PTS;
	association_p->state = ASSOCIATION_CACHING_INVALID;
	strncpy(association_p->name, name, STM_REGISTRY_MAX_TAG_SIZE - 1);
	list_add_tail(&association_p->lh, &switch_p->associate_list);
	mutex_init(&association_p->lock);
	INIT_LIST_HEAD(&association_p->associated_input_pad_list);

	pr_info("%s:%d> Association: %s HDL:0x%p initialized\n",
			__func__, __LINE__, name, association_p);

	*association_h = association_p;

	return ret;
}


int switch_association_delete(struct switch_association_s *association)
{

	pr_info("%s:%d> Del Association:0x%p\n", __func__, __LINE__,
			association);
	list_del(&association->lh);
	mutex_destroy(&association->lock);
	kfree(association);

	return 0;
}


struct switch_association_s *switch_get_association_handle(const char *name)
{
	struct list_head *associate_list_head;
	struct switch_global *switch_p = &g_switch;
	struct switch_association_s *association_node;
	int ret = 0;

	associate_list_head = &switch_p->associate_list;

	list_for_each_entry(association_node, associate_list_head, lh) {
		ret = strcmp(association_node->name, name);
		if (!ret)
			return association_node;
	}
	return NULL;
}

static int __init stm_switch_init(void)
{
	struct switch_global *switch_p = &g_switch;
	int ret = 0;

	INIT_LIST_HEAD(&switch_p->sel_list);
	INIT_LIST_HEAD(&switch_p->associate_list);
	mutex_init(&switch_p->lock);

	ret = __register_classes(switch_p);

	return 0;
}
module_init(stm_switch_init);

static void __exit stm_switch_term(void)
{
	struct switch_global *switch_p = &g_switch;

	__unregister_classes(switch_p);
}
module_exit(stm_switch_term);
MODULE_LICENSE("GPL");

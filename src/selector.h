/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __SELECTOR_H
#define __SELECTOR_H

#include "switch_obj.h"
#include "input_pad.h"

#define SWITCH_CLASS_SELECTOR		"selector"

struct stm_selector_s {
	struct switch_obj obj;

	int data_interface_type;
	/*List of all input pad obj attached to this selector*/
	struct list_head in_pad_list;

	/*defines the nature of the buffer? secure/unsecure*/
	bool is_secure;

	struct obj_pad_time_info_s time_info;
	stm_object_h sink_obj;

	stm_data_interface_push_sink_t data_push_interface;
	stm_se_clock_data_point_interface_push_sink_t clk_push_interface;
};

int selector_register_class(struct switch_obj_class *selector_class);
int selector_unregister_class(struct switch_obj_class *selector_class);
struct stm_selector_s *get_selector_from_obj(struct switch_obj *sel_obj);
int get_sel_obj_from_hdl(stm_object_h hdl, struct switch_obj **obj);
bool is_selector_allocated(const char *name, struct switch_obj **sel_obj);
int inc_selector_ref_count(struct switch_obj *sel_obj);
int dec_selector_ref_count(struct switch_obj *sel_obj);
int selector_new(const char *name, struct switch_obj **selector);
struct list_head *selector_get_in_pad_list(struct switch_obj *sel_obj);

int selector_set_clock_data_point(struct switch_obj *sel_obj,
				stm_se_time_format_t time_format,
				bool new_seq, unsigned long long source_time,
				unsigned long long system_time);

int selector_push_data(struct switch_obj *sel_obj,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks);

int selector_set_secure_path_status(struct switch_obj *sel_obj,
		bool secure_flag);

#endif

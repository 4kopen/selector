/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __PAD_PARSER_H
#define __PAD_PARSER_H

#define PAD_PES_START_CODE_ARR_SZ	6
#define PAD_PES_START_CODE_MAP_SZ	8

#define PAD_PES_PKT_STATUS_NOK -1
#define PAD_PES_PKT_STATUS_IDLE 0
#define PAD_PES_PKT_STATUS_IN_PROGRESS 1
#define PAD_PES_PKT_STATUS_OK 2

struct pad_pes_pkt_s {
	uint8_t state;
	uint8_t es_state;
	int status;
	uint8_t *cur_pos;
	uint8_t *pes_header_pos;
	uint8_t *next_pes_header_pos;
	uint8_t *cur_es_pos;

	uint32_t skip_leading_bytes;

	uint8_t pkt_len_byte_stream[2];
	uint32_t pkt_len;

	uint8_t pts_flag;
	uint8_t pts_byte_stream[5];
	bool has_pts;
	unsigned long long pts;

	uint32_t sc_map[PAD_PES_START_CODE_MAP_SZ];

	uint8_t *sc_pos;
	bool has_sc;

	uint32_t es_cnt;

	uint32_t bytes_to_skip_in_hdr;
	uint8_t pkt_hdr_len;

	uint32_t cur_len;
};

int pes_mp2_parse_pkt(struct pad_pes_pkt_s *pes_pkt,
			uint32_t len, uint32_t *sc_map,
			uint8_t num_sc, uint8_t *exit);
int pes_mp2_validate_header(uint8_t *pes, uint32_t len);
int pes_mp2_get_sc_map(uint8_t *sc, uint32_t len,
		uint32_t *sc_map, uint8_t map_sz);


#endif

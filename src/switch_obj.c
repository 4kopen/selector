/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include <stm_registry.h>

#include <linux/string.h>
#include <linux/errno.h>

#include "switch_obj.h"
#include "switch_global.h"

int static __is_hdl_valid(stm_object_h hdl, struct switch_obj_class *obj_class);

int switch_obj_init(struct switch_obj *obj, enum switch_obj_type_e type,
		char *name, struct switch_obj *parent,
		struct switch_obj_class *type_obj)
{
	int err;

	/* Check if we have reached the maximum number for this object type */
	if (atomic_read(&type_obj->count) >= type_obj->max) {
		pr_err("Maximum number of obj for class %d reached\n",
				type_obj->class);
		err = -ENOMEM;
		goto error;
	}

	obj->type = type;
	obj->state = SWITCH_OBJ_STOPPED;
	obj->parent = parent;
	obj->class_obj = type_obj;
	strncpy(obj->name, name, STM_REGISTRY_MAX_TAG_SIZE - 1);
	mutex_init(&obj->lock);

	/* Add object and core attributes to the registry */
	err = stm_registry_add_instance(
			parent ? parent : STM_REGISTRY_INSTANCES,
			type_obj, name, obj);
	if (err) {
		pr_err("Failed to add object %s to registry (%d)\n", name,
				err);
		goto err0;
	}

	err = stm_registry_add_attribute(obj, "type", STM_REGISTRY_UINT32,
			&obj->type, sizeof(obj->type));
	if (err) {
		pr_err("Failed to add type attr to obj %s (%d)\n", name, err);
		goto err1;
	}

	err = stm_registry_add_attribute(obj, "state", STM_REGISTRY_UINT32,
			&obj->state, sizeof(obj->state));
	if (err) {
		pr_err("Failed to add state attr to obj %s (%d)\n", name, err);
		goto err1;
	}

	/* Increment the total count of the object class (type object) */
	atomic_inc(&type_obj->count);
	atomic_set(&obj->ref, 1);

	return 0;

err1:
	if (stm_registry_remove_object(obj) != 0)
		pr_warn("couldn't remove object\n");
err0:
	mutex_destroy(&obj->lock);
error:
	return err;
}


int switch_obj_deinit(struct switch_obj *obj)
{
	int err;
	/* Remove object from the registry. Return error immediately, since
	 * this probably means that the object is busy and cannot be removed */
	err = stm_registry_remove_object(obj);
	if (err) {
		pr_err("Failed to remove object %s (%d)\n", obj->name, err);
		return err;
	}
	mutex_destroy(&obj->lock);
	atomic_dec(&obj->class_obj->count);

	return 0;
}

static int __is_hdl_valid(stm_object_h hdl, struct switch_obj_class *obj_class)
{
	stm_object_h type_object;
	int err = stm_registry_get_object_type(hdl, &type_object);

	if (err == 0 && (type_object == obj_class))
		return 0;

	return -EINVAL;
}

int get_obj_from_hdl(stm_object_h hdl,
		struct switch_obj_class *obj_class,
		struct switch_obj **obj)
{
	int err = __is_hdl_valid(hdl, obj_class);

	if (!err)
		*obj = (struct switch_obj *)hdl;

	return err;
}

enum switch_obj_class_e get_class_type_from_obj(struct switch_obj *obj)
{
	return obj->class_obj->class;
}

enum switch_obj_type_e get_obj_type(struct switch_obj *obj)
{
	return obj->type;
}

void set_obj_type(struct switch_obj *obj, enum switch_obj_type_e type)
{
	obj->type = type;
}

/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/*!
 * \file switch_obj.h
 *
 * Defines a set of functions for reading and writing attributes of switch
 * objects.
 *
 * Members of switch objects should not be accessed directly, except by the
 * functions declared in this header. This allows the implementation of the
 * switch internal object model to be changed, with only limited impact on the
 * codebase
 */
#ifndef __SWITCH_OBJ_H
#define __SWITCH_OBJ_H

#include <stm_common.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/stddef.h>

#include <stm_se.h>
/*!
 * \brief Selector object types
 *
 */
enum switch_obj_type_e {
	SWITCH_OBJ_TYPE_VIDEO,
	SWITCH_OBJ_TYPE_AUDIO,
	SWITCH_OBJ_TYPE_PCR,
	SWITCH_OBJ_TYPE_UNKNOWN
};

/*!
 * \brief Selector object states
 */
enum switch_obj_state_e {
	/*! An object in stopped state*/
	SWITCH_OBJ_STOPPED = 0,
	/*! Object in pause state */
	SWITCH_OBJ_PAUSED,
	/*! An object in started state*/
	SWITCH_OBJ_STARTED,
};


/*!
 * \brief Selector object classes
 */
enum switch_obj_class_e {
	SWITCH_OBJ_CLASS_SELECTOR,
	SWITCH_OBJ_CLASS_INPUT_PAD,
	SWITCH_OBJ_CLASS_OUTPUT_PAD,
};


/*!
 * \brief Represents a single object class
 */
struct switch_obj_class {
	enum switch_obj_class_e class;
	uint32_t max;
	atomic_t count;
};

struct obj_pad_time_info_s {
	stm_se_time_format_t time_format;
	bool new_seq;
	unsigned long long source_time;
	unsigned long long system_time;
};

struct switch_obj {
	enum switch_obj_type_e type;
	struct switch_obj_class *class_obj;
	enum switch_obj_state_e state;
	struct switch_obj *parent;
	char name[STM_REGISTRY_MAX_TAG_SIZE];
	struct list_head lh;
	struct mutex lock;
	struct switch_obj_ops *ops;
	atomic_t ref;
};

/*!
 * \brief vtable of operations performed on all struct te_obj operations
 *
 * No function pointer in an instance of this vtable may be NULL
 */
struct switch_obj_ops {
	int (*delete)(struct switch_obj *obj);
	int (*set_control)(struct switch_obj *obj, uint32_t ctrl,
			const void *buf, uint32_t size);
	int (*get_control)(struct switch_obj *obj, uint32_t ctrl, void *buf,
			uint32_t size);
	int (*attach)(struct switch_obj *obj, stm_object_h target);
	int (*detach)(struct switch_obj *obj, stm_object_h target);
};


int switch_obj_init(struct switch_obj *obj, enum switch_obj_type_e type,
		char *name, struct switch_obj *parent,
		struct switch_obj_class *type_obj);
int switch_obj_deinit(struct switch_obj *obj);
enum switch_obj_class_e get_class_type_from_obj(struct switch_obj *obj);
int get_obj_from_hdl(stm_object_h hdl, struct switch_obj_class *obj_class,
		struct switch_obj **obj);
enum switch_obj_type_e get_obj_type(struct switch_obj *obj);
void set_obj_type(struct switch_obj *obj, enum switch_obj_type_e type);

/* Set/get control helpers */
static inline int __set_control(void *ctrl, size_t ctrl_size, const void *buf,
		size_t buf_size)
{
	if (!buf)
		return -EINVAL;

	if (buf_size == ctrl_size)
		memcpy(ctrl, buf, ctrl_size);
	else
		return -EINVAL;
	return 0;
}
#define SET_CONTROL(ctrl, buf, size) \
	__set_control(&(ctrl), sizeof(ctrl), buf, size)

static inline int __get_control(void *ctrl, size_t ctrl_size, void *buf,
		size_t buf_size)
{
	if (!buf)
		return -EINVAL;

	if (buf_size >= ctrl_size)
		memcpy(buf, ctrl, ctrl_size);
	else
		return -EINVAL;
	return 0;
}
#define GET_CONTROL(ctrl, buf, size) \
	__get_control(&(ctrl), sizeof(ctrl), buf, size)


#endif

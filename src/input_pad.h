/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __INPUT_PAD_H
#define __INPUT_PAD_H

#include "switch_obj.h"
#include "switch_global.h"
#include "stm_data_interface.h"
#include "stm_registry.h"
#include "pad_parser.h"

#define SWITCH_CLASS_INPUT_PAD	"selector_input_pad"

struct cache_node_s {
	struct list_head lh;

	bool sc_present;
	bool pts_valid;
	uint64_t pts;

	uint32_t cache_len;

	/*last pes packet parsed*/
	struct pad_pes_pkt_s pkt;
	uint8_t *pes_start;
	unsigned long long ktime;
};


struct input_pad_s {
	struct switch_obj obj;

	atomic_t usr_connect_mode;
	atomic_t usr_rebase_policy;

	int connect_mode;
	int rebase_policy;
	int cache_unit_cnt;
	bool is_secure;
	bool caching_done;
	bool pcr_invalid;

	struct obj_pad_time_info_s time_info;
	struct switch_obj *output_pad_obj;
	struct stm_object_h *src_obj;

	struct list_head associate_lh;
	struct switch_association_s *association;

	char start_code[PAD_PES_START_CODE_ARR_SZ];
	uint32_t sc_map[PAD_PES_START_CODE_MAP_SZ];
	uint8_t num_sc;

	struct cache_node_s cache_head;
	int cache_list_cnt;

	uint64_t latest_pcr;
	int64_t stream_offset;

	struct stm_data_block block[2];
	uint32_t block_count;

	uint32_t gop_count;
	uint32_t buffer_size;
	uint32_t buf_limit;
	uint8_t exit;
};

int input_pad_register_class(struct switch_obj_class *input_pad_class);
int input_pad_unregister_class(struct switch_obj_class *input_pad_class);
int input_pad_new(struct switch_obj *sel_obj, struct switch_obj **pad_obj);
int input_pad_share_data_with_associate_pad(struct switch_obj *pad_obj,
		struct obj_pad_time_info_s *time_info);
int get_in_pad_from_hdl(stm_object_h pad, struct input_pad_s **input_pad);
int input_pad_add_to_association(struct input_pad_s *in_pad,
		struct switch_association_s *association);
int input_pad_remove_from_associaton(struct input_pad_s *in_pad);
int input_pad_set_start_code(struct input_pad_s *input_pad,
		const char *start_code, unsigned int len);

#endif

/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/**
 *  @File   pad_parser.c
 *  @brief
 *
 *
 *
 * */

#include "stm_selector.h"
#include "switch_global.h"
#include "selector.h"
#include "input_pad.h"

enum _parser_pes_pkt_state_e {
	_PES_MP2_SYNC_0_00,
	_PES_MP2_SYNC_1_00,
	_PES_MP2_SYNC_2_01,
	_PES_MP2_SYNC_3_STREAMID,
	_PES_MP2_PKT_LEN_1,
	_PES_MP2_PKT_LEN_2,
	_PES_MP2_PACKET_RES1,
	_PES_MP2_PACKET_RES2,
	_PES_MP2_HDR_LEN,
	_PES_MP2_STMARKER_PRIVATE_DATA,
	_PES_MP2_STDISCONT_MARKER,
	_PES_MP2_PTS_1,
	_PES_MP2_PTS_2,
	_PES_MP2_PTS_3,
	_PES_MP2_PTS_4,
	_PES_MP2_PTS_5,
	_PES_MP2_SKIP_HDR_EXT,
	_PES_MP2_SKIP_PES,
	_PES_MP2_SKIP_HEADER,
	_PES_MP2_ES_DATA,
	_PES_MP2_DONE
};

enum _parser_es_pkt_state_e {
	_ES_SYNC_0_00,
	_ES_SYNC_1_00,
	_ES_SYNC_2_01,
	_ES_SYNC_3_START_CODE,
};

static inline void _pes_mp2_parse_sync0(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static inline void _pes_mp2_parse_sync1(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static inline void _pes_mp2_parse_sync2(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static inline void _pes_mp2_parse_pkt_len1(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static inline void _pes_mp2_parse_pkt_len2(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static inline void _pes_mp2_skip_byte(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static inline void _pes_mp2_check_pts_flag(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static inline void _pes_mp2_check_pts_flag(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static inline void _pes_mp2_parse_hdr_len(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static inline void _pes_mp2_parse_pts_bytes(struct pad_pes_pkt_s *pes_pkt,
			uint8_t pts_index, uint32_t *byte_read);
static inline void _pes_mp2_cal_pts(struct pad_pes_pkt_s *pes_pkt);
static  inline void _pes_mp2_skip_hdr(struct pad_pes_pkt_s *pes_pkt,
			uint32_t len, uint32_t *byte_read);

static int _pes_mp2_parse_es(struct pad_pes_pkt_s *pes_pkt, uint32_t len,
			uint32_t *byte_read, uint32_t *sc_map, uint8_t num_sc);
static  inline void _es_parse_sync0(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static  inline void _es_parse_sync1(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read);
static  inline void _es_parse_start_code(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read, uint32_t *sc_map, uint8_t num_sc);

static inline void _pes_mp2_parse_sync0(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	uint8_t *pes_ptr;
	pes_ptr = pes_pkt->cur_pos + *byte_read;
	if (*(pes_ptr) == 0x00) {
		pes_pkt->state = _PES_MP2_SYNC_1_00;
		pes_pkt->pes_header_pos = pes_ptr;
	} else {
		pes_pkt->skip_leading_bytes++;
	}

	(*byte_read)++;
}

static inline void _pes_mp2_parse_sync1(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;
	pes_pkt->state = _PES_MP2_SYNC_0_00;
	pr_debug("%s:%d> State:PES_MP2_SYNC_1_00 pkt cur val:0x%02x\n",
			__func__, __LINE__, *(pes_ptr));
	if (*(pes_ptr) == 0x00) {
		pes_pkt->state = _PES_MP2_SYNC_2_01;
		pes_pkt->pes_header_pos = pes_ptr;
	} else {
		pes_pkt->skip_leading_bytes += 2;
	}
	(*byte_read)++;
}

static inline void _pes_mp2_parse_sync2(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;
	pes_pkt->state = _PES_MP2_SYNC_0_00;

	pr_debug("%s:%d> State:PES_MP2_SYNC_2_01 pkt cur val:0x%02x\n",
			__func__, __LINE__, *(pes_ptr));

	if (*(pes_ptr) == 0x01) {
		pes_pkt->state = _PES_MP2_SYNC_3_STREAMID;
		(*byte_read)++;
	} else {
		pes_pkt->state = _PES_MP2_SYNC_1_00;
		pes_pkt->skip_leading_bytes++;
	}
}


static inline void _pes_mp2_parse_pkt_len1(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;
	pes_pkt->state = _PES_MP2_PKT_LEN_2;

	pes_pkt->pkt_len_byte_stream[0] = *pes_ptr;
	(*byte_read)++;
}


static inline void _pes_mp2_parse_pkt_len2(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;
	pes_pkt->state = _PES_MP2_PACKET_RES1;

	pes_pkt->pkt_len_byte_stream[1] = *pes_ptr;
	pes_pkt->pkt_len = (pes_pkt->pkt_len_byte_stream[0] << 8) +
				pes_pkt->pkt_len_byte_stream[1];
	(*byte_read)++;
}

static inline void _pes_mp2_skip_byte(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	(*byte_read)++;
}

static inline void _pes_mp2_check_pts_flag(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;
	pes_pkt->state = _PES_MP2_HDR_LEN;
	pes_pkt->pts_flag = ((*(pes_ptr)  & 0xC0)>>6);
	pr_debug("%s:%d> Pkt:0x%p PTS Flag:0x%02x\n",
			__func__, __LINE__, pes_pkt, pes_pkt->pts_flag);
	(*byte_read)++;
}

static inline void __cal_skip_hdr_len_with_pts(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	/*PTS only*/
	if (pes_pkt->pkt_hdr_len<5) {
		/* 5 Because of Fixed 5 for PTS always*/
		pes_pkt->state = _PES_MP2_SYNC_0_00;
		pr_info("%s:%d> Inavlid Length: %d\n",
				__func__, __LINE__,
				pes_pkt->pkt_hdr_len);


	} else {
		/* 5 Because of Fixed 5 for PTS always*/
		pes_pkt->bytes_to_skip_in_hdr = pes_pkt->pkt_hdr_len-5;
		pes_pkt->state = _PES_MP2_PTS_1;
		pr_debug("%s:%d> Skip HDRLen: %d\n",
				__func__, __LINE__,
				pes_pkt->bytes_to_skip_in_hdr);
		pes_pkt->status = PAD_PES_PKT_STATUS_OK;
	}
}



static inline void __cal_skip_hdr_len_with_dts(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	//PTS & DTS
	if (pes_pkt->pkt_hdr_len < 10) {
		/* 10 Because of Fixed 10 for DTS+PTS always*/
		pes_pkt->state = _PES_MP2_SYNC_0_00;
		pr_info("%s:%d> Inavlid Length: %d\n",
				__func__, __LINE__,
				pes_pkt->pkt_hdr_len);
	} else {
		/* 5 Because of Fixed 5 for PTS always*/
		pes_pkt->bytes_to_skip_in_hdr = pes_pkt->pkt_hdr_len-5;
		pes_pkt->state = _PES_MP2_PTS_1;
		pes_pkt->status = PAD_PES_PKT_STATUS_OK;
	}

}


static inline void _pes_mp2_parse_hdr_len(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;
	pes_pkt->pkt_hdr_len = *pes_ptr;
	if (pes_pkt->pkt_hdr_len) {
		if (pes_pkt->pts_flag == 0x2) {
				__cal_skip_hdr_len_with_pts(pes_pkt, byte_read);
		} else if (pes_pkt->pts_flag == 0x3) {
			//PTS & DTS
				__cal_skip_hdr_len_with_dts(pes_pkt, byte_read);
		} else {
			pes_pkt->bytes_to_skip_in_hdr =  pes_pkt->pkt_hdr_len;
			pes_pkt->state = _PES_MP2_SKIP_HDR_EXT;
		}
	} else {
		/* There is 0 byte header so ES data will start now */
		pes_pkt->bytes_to_skip_in_hdr=0;
		pes_pkt->state = _PES_MP2_SKIP_HEADER;
	}
	(*byte_read)++;
}

static inline void _pes_mp2_parse_pts_bytes(struct pad_pes_pkt_s *pes_pkt,
			uint8_t pts_index, uint32_t *byte_read)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;
	pes_pkt->state = _PES_MP2_PTS_1 + (pts_index + 1);
	pes_pkt->pts_byte_stream[pts_index] = *pes_ptr;
	(*byte_read)++;
}

static inline void _pes_mp2_cal_pts(struct pad_pes_pkt_s *pes_pkt)
{
	pes_pkt->pts = ((uint64_t)((pes_pkt->pts_byte_stream[0] >> 1) & 0x07) << 30) |
			((uint64_t)( pes_pkt->pts_byte_stream[1] ) << 22) |
			((uint64_t)((pes_pkt->pts_byte_stream[2] >> 1) & 0x7f) << 15) |
			((uint64_t)(pes_pkt->pts_byte_stream[3] ) <<  7) |
			((uint64_t)((pes_pkt->pts_byte_stream[4] >> 1) & 0x7f) );

	pr_debug("%s:%d> Pkt:0x%p PTS:%llu \n",
			__func__, __LINE__, pes_pkt, pes_pkt->pts);

	pes_pkt->state = _PES_MP2_SKIP_HEADER;
	pes_pkt->has_pts = true;
}

static  inline void _pes_mp2_skip_hdr(struct pad_pes_pkt_s *pes_pkt,
			uint32_t len, uint32_t *byte_read)
{
	uint32_t min_byte_to_skip = min_t(uint32_t, (len-*byte_read),
					pes_pkt->bytes_to_skip_in_hdr);

	pes_pkt->bytes_to_skip_in_hdr -= min_byte_to_skip;

	pr_debug("%s:%d> Pkt:0x%p Skipping Bytes:%d\n",
			__func__, __LINE__, pes_pkt, min_byte_to_skip);

	while (min_byte_to_skip) {
		_pes_mp2_skip_byte(pes_pkt, byte_read);
		min_byte_to_skip--;
	}

	if (pes_pkt->bytes_to_skip_in_hdr == 0) {
		pes_pkt->state = _PES_MP2_ES_DATA;
		pes_pkt->es_state = _ES_SYNC_0_00;
		pr_debug("%s:%d> Pkt:0x%p Skipping Complete\n",
			__func__, __LINE__, pes_pkt);

	}
}

static  inline void _es_parse_sync0(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;
	if (*pes_ptr == 0x00) {
		pes_pkt->es_state = _ES_SYNC_1_00;
		pes_pkt->cur_es_pos = pes_ptr;
	}

	(*byte_read)++;
}


static  inline void _es_parse_sync1(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;
	pes_pkt->es_state = _ES_SYNC_0_00;
	if (*pes_ptr == 0x00) {
		pes_pkt->es_state = _ES_SYNC_2_01;
	}
	(*byte_read)++;
}


static  inline void _es_parse_sync2(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;
	pes_pkt->es_state = _ES_SYNC_0_00;
	if (*pes_ptr == 0x01) {
		pes_pkt->es_state = _ES_SYNC_3_START_CODE;
	} else if (*pes_ptr == 0x00) {
		pes_pkt->es_state = _ES_SYNC_2_01;
	}
	(*byte_read)++;
}

static void __get_sc_map(uint8_t *pos,
		uint8_t *map_index, uint32_t *map_val)
{
	*map_val = 0;
	*map_index = 0;
	if (*pos) {
		*map_index = (*pos)/32;
		*map_val = (1 << ((*pos)%32));
	}
}

static  inline void _es_parse_start_code(struct pad_pes_pkt_s *pes_pkt,
			uint32_t *byte_read, uint32_t *sc_map, uint8_t num_sc)
{
	uint8_t map_index = 0;
	uint8_t *pes_ptr;
	uint32_t map_val;

	pes_pkt->es_cnt++;

	/* If no start code requested then we are done with parsing.
	 * */
	if(!num_sc) {
                pes_pkt->state = _PES_MP2_DONE;
		return;
	}

	pes_ptr = pes_pkt->cur_pos + *byte_read;

	/* Reset the ES state to _ES_SYNC_0_00 as this PES might
	 * contain more than one ES
	 * Parse until end of length. Assuming that end of length is one PES.
	 * */

	pes_pkt->es_state = _ES_SYNC_0_00;
	__get_sc_map(pes_ptr, &map_index, &map_val);
	pr_debug("%s:%d> Pkt:0x%p SC MapVal:0x%x Data MapVal:0x%x\n",
			__func__, __LINE__, pes_pkt,
			sc_map[map_index], map_val);

	if (sc_map[map_index] & map_val) {
		pes_pkt->has_sc = true;
		pes_pkt->sc_pos = pes_ptr;
		pr_debug("%s:%d> Pkt:0x%p Start code 0x%02x found\n",
					__func__, __LINE__, pes_pkt, *pes_ptr);
		/* If start code is found then we need this PES
		 * So set the parsing to _PES_MP2_DONE
		 * */
		pes_pkt->state = _PES_MP2_DONE;
	}
}


static int _pes_mp2_parse_es(struct pad_pes_pkt_s *pes_pkt, uint32_t len,
			uint32_t *byte_read, uint32_t *sc_map, uint8_t num_sc)
{
	uint8_t *pes_ptr;

	pes_ptr = pes_pkt->cur_pos + *byte_read;

	switch(pes_pkt->es_state) {
	case _ES_SYNC_0_00:
		pes_pkt->pes_header_pos = pes_ptr;
		pr_debug("%s:%d> Pkt:0x%p ES State:%s byte: 0x%02x\n",
				__func__, __LINE__, pes_pkt,
				"ES_SYNC_0_00", *pes_ptr);
		_es_parse_sync0(pes_pkt, byte_read);
		break;
	case _ES_SYNC_1_00:
		pr_debug("%s:%d> Pkt:0x%p ES State:%s byte: 0x%02x\n",
				__func__, __LINE__, pes_pkt,
				"ES_SYNC_1_00", *pes_ptr);
		_es_parse_sync1(pes_pkt, byte_read);
		break;
	case _ES_SYNC_2_01:
		pr_debug("%s:%d> Pkt:0x%p ES State:%s byte: 0x%02x\n",
				__func__, __LINE__, pes_pkt,
				"ES_SYNC_2_01", *pes_ptr);
		_es_parse_sync2(pes_pkt, byte_read);
		break;
	case _ES_SYNC_3_START_CODE:
		pr_debug("%s:%d> Pkt:0x%p ES State:%s byte: 0x%02x\n",
				__func__, __LINE__, pes_pkt,
				"ES_SYNC_3_START_CODE", *pes_ptr);
		_es_parse_start_code(pes_pkt, byte_read, sc_map, num_sc);
		break;
	}

	return 0;
}


int pes_mp2_validate_header(uint8_t *pes, uint32_t len)
{
	unsigned char * pes_data = (unsigned char *)pes;

	/*check if we have enough bytes to parse the header*/
	if (len < 3)
		return -EINVAL;

	if ((pes_data[0] == 0) &&
		(pes_data[1] == 0) &&
		(pes_data[2] == 1)) {
		return 0;
	}

	/* error condition */
	return -EINVAL;
}


int pes_mp2_parse_pkt(struct pad_pes_pkt_s *pes_pkt,
			uint32_t len, uint32_t *sc_map,
			uint8_t num_sc, uint8_t *exit)
{
	uint32_t byte_read = 0;
	int first = 1;
	uint8_t *pes_ptr;

	pes_pkt->cur_len = len;

	while (*exit == 0 && byte_read < len) {
		if (first ==0 && (pes_pkt->state) == _PES_MP2_SYNC_0_00) {
			pes_pkt->status = PAD_PES_PKT_STATUS_NOK;
			break;
		}

		pes_ptr = pes_pkt->cur_pos + byte_read;

		switch(pes_pkt->state) {
		case _PES_MP2_SYNC_0_00:
			pes_pkt->pes_header_pos = pes_ptr;
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_SYNC_0_00", *pes_ptr);
			_pes_mp2_parse_sync0(pes_pkt, &byte_read);
			break;
		case _PES_MP2_SYNC_1_00:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_SYNC_1_00", *pes_ptr);
			_pes_mp2_parse_sync1(pes_pkt, &byte_read);
			break;
		case _PES_MP2_SYNC_2_01:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_SYNC_2_01", *pes_ptr);
			_pes_mp2_parse_sync2(pes_pkt, &byte_read);
			break;
		case _PES_MP2_SYNC_3_STREAMID:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_SYNC_3_STREAMID", *pes_ptr);
			_pes_mp2_skip_byte(pes_pkt, &byte_read);
			pes_pkt->state = _PES_MP2_PKT_LEN_1;
			break;
		case _PES_MP2_PKT_LEN_1:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_PKT_LEN_1", *pes_ptr);
			_pes_mp2_parse_pkt_len1(pes_pkt, &byte_read);
			break;
		case _PES_MP2_PKT_LEN_2:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_PKT_LEN_2", *pes_ptr);
			_pes_mp2_parse_pkt_len2(pes_pkt, &byte_read);
			break;
		case _PES_MP2_PACKET_RES1:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_PACKET_RES1", *pes_ptr);
			_pes_mp2_skip_byte(pes_pkt, &byte_read);
			pes_pkt->state = _PES_MP2_PACKET_RES2;
			break;
		case _PES_MP2_PACKET_RES2:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_PACKET_RES2", *pes_ptr);
			_pes_mp2_check_pts_flag(pes_pkt, &byte_read);
			break;
		case _PES_MP2_HDR_LEN:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_HDR_LEN", *pes_ptr);
			_pes_mp2_parse_hdr_len(pes_pkt, &byte_read);
			break;
		case _PES_MP2_PTS_1:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_PTS_1", *pes_ptr);
			_pes_mp2_parse_pts_bytes(pes_pkt, 0, &byte_read);
			break;
		case _PES_MP2_PTS_2:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_PTS_2", *pes_ptr);
			_pes_mp2_parse_pts_bytes(pes_pkt, 1, &byte_read);
			break;
		case _PES_MP2_PTS_3:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_PTS_3", *pes_ptr);
			_pes_mp2_parse_pts_bytes(pes_pkt, 2, &byte_read);
			break;
		case _PES_MP2_PTS_4:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_PTS_4", *pes_ptr);
			_pes_mp2_parse_pts_bytes(pes_pkt, 3, &byte_read);
			break;
		case _PES_MP2_PTS_5:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_PTS_5", *pes_ptr);
			_pes_mp2_parse_pts_bytes(pes_pkt, 4, &byte_read);
			_pes_mp2_cal_pts(pes_pkt);
		    break;
		case _PES_MP2_SKIP_HEADER:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_SKIP_HEADER", *pes_ptr);
			_pes_mp2_skip_hdr(pes_pkt, len, &byte_read);
		    break;
		case _PES_MP2_ES_DATA:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_ES_DATA", *pes_ptr);
			_pes_mp2_parse_es(pes_pkt, len, &byte_read,
					sc_map, num_sc);
		    break;
		case _PES_MP2_DONE:
			pr_debug("%s:%d> Pkt:0x%p PES State:%s byte: 0x%02x\n",
					__func__, __LINE__, pes_pkt,
					"PES_MP2_DONE", *pes_ptr);

			return 0;
		    break;
		default:
			pr_warn("%s:%d> Pkt:0x%p PES State:INVALID STATE(%d)\n",
					__func__, __LINE__,
					pes_pkt, pes_pkt->state);
			_pes_mp2_skip_byte(pes_pkt, &byte_read);
		}
		first = 0;

		/* If exit is requested, put pkt status as OK because
		 * pad will be used in pass through mode now.
		 * */
		if (*exit == 1)
			pes_pkt->status = PAD_PES_PKT_STATUS_OK;

	}
	//TODO - Add actual return value
	pr_debug("%s:%d> Pkt:0x%p Exit Parsing ByteRead: %d\n",
					__func__, __LINE__,
					pes_pkt, byte_read);

	return 0;
}


int pes_mp2_get_sc_map(uint8_t *sc, uint32_t num_sc,
		uint32_t *sc_map, uint8_t map_sz)
{
	int i = 0;
	uint32_t val;
	uint8_t map_index = 0;

	for (; i < num_sc; i++){
		__get_sc_map(&sc[i], &map_index, &val);
		sc_map[map_index] |= val;
		pr_debug("%s:%d> SC:0x%02x MapVal:0x%x MapIndex:%d\n",
				__func__, __LINE__,
				sc[i], sc_map[map_index], map_index);
	}
	return 0;
}

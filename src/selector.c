/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include "stm_selector.h"
#include "selector.h"

#define SWITCH_CLASS_SELECTOR_MAX	10

static int __update_sel_obj_type(struct stm_selector_s *sel);
static int __attach_to_data_push_sink(struct stm_selector_s *selector,
		stm_object_h sink_object,
		stm_object_h  target_type);
static int __attach_to_clk_push_sink(struct stm_selector_s *selector,
		stm_object_h sink_object,
		stm_object_h  target_type);
static int __detach_to_data_push_sink(struct stm_selector_s *selector,
		stm_object_h sink_object);
static int __detach_to_clk_push_sink(struct stm_selector_s *selector,
		stm_object_h sink_object);

static int selector_attach(struct switch_obj *sel_obj,
		stm_object_h sink_object);
static int selector_detach(struct switch_obj *sel_obj,
		stm_object_h sink_object);
static int selector_delete(struct switch_obj *sel_obj);
static int selector_set_control(struct  switch_obj *sel_obj, uint32_t control,
		const void *buf, uint32_t size);
static int selector_get_control(struct switch_obj *sel_obj, uint32_t control,
		void *buf, uint32_t size);
static int __calculate_byte_read(struct stm_data_block *block_list,
			uint32_t block_count);

static struct switch_obj_ops selector_ops = {
	.delete = &selector_delete,
	.set_control = &selector_set_control,
	.get_control = &selector_get_control,
	.attach = &selector_attach,
	.detach = &selector_detach,
};

int selector_register_class(struct switch_obj_class *selector_class)
{
	int ret = 0;

	ret = stm_registry_add_object(STM_REGISTRY_TYPES, "selector",
			selector_class);
	if (ret)
		pr_err("Failed to register selector class\n");

	selector_class->class = SWITCH_OBJ_CLASS_SELECTOR;
	selector_class->max = SWITCH_CLASS_SELECTOR_MAX;
	atomic_set(&selector_class->count, 0);

	return ret;
}

int selector_unregister_class(struct switch_obj_class *selector_class)
{
	int ret = 0;

	ret = stm_registry_remove_object(selector_class);
	if (ret)
		pr_warn("Failed to remove demux class\n");

	return ret;
}

int get_sel_obj_from_hdl(stm_object_h hdl, struct switch_obj **obj)
{
	int err;
	stm_object_h obj_class_type;
	struct switch_obj_class *sel_class;

	*obj = NULL;
	sel_class = get_class_from_class_type(SWITCH_OBJ_CLASS_SELECTOR);
	err = stm_registry_get_object_type(hdl, &obj_class_type);
	if (!err) {
		if (obj_class_type == sel_class)
			*obj = (struct switch_obj *)hdl;
		else
			err = -EINVAL;
	}
	return err;
}




/*!
 * \brief return selector pointer from an obj
 */
struct stm_selector_s *get_selector_from_obj(struct switch_obj *sel_obj)
{
	enum switch_obj_class_e obj_class;

	if (!sel_obj)
		return NULL;

	obj_class = get_class_type_from_obj(sel_obj);
	if (obj_class != SWITCH_OBJ_CLASS_SELECTOR)
		return NULL;

	return container_of(sel_obj, struct stm_selector_s, obj);
}


bool is_selector_allocated(const char *name, struct switch_obj **sel_obj)
{
	struct list_head *sel_list;
	int ret = 0;
	struct switch_obj *cur_sel_obj;

	ret = switch_global_get_sel_list(&sel_list);

	/* Loop on all allocated selectors */
	list_for_each_entry(cur_sel_obj, sel_list, lh) {
		ret = strcmp(cur_sel_obj->name, name);
		if (!ret) {
			*sel_obj = cur_sel_obj;
			return true;
		}
	}
	return false;
}

int inc_selector_ref_count(struct switch_obj *sel_obj)
{
	/* Increment the total count of the object class (type object) */
	atomic_inc(&sel_obj->ref);
	return 0;
}

int dec_selector_ref_count(struct switch_obj *sel_obj)
{
	int cnt;

	cnt = atomic_read(&sel_obj->ref);

	if (cnt >= 1) {
		/* decrement the ref count for this obj*/
		atomic_dec(&sel_obj->ref);
		cnt = atomic_read(&sel_obj->ref);
	}

	return cnt;
}


int selector_new(const char *name, struct switch_obj **selector)
{
	struct stm_selector_s *sel;
	int err;
	struct switch_obj_class *sel_class;
	struct list_head *sel_list;

	/* Sanity check name. The registry will do this for us later anyway.
	 * This check is so that we can comply with our documented return code
	 * of -ENAMETOOLONG */
	if (strnlen(name, STM_REGISTRY_MAX_TAG_SIZE) >
			STM_REGISTRY_MAX_TAG_SIZE - 1)
		return -ENAMETOOLONG;

	sel = kzalloc(sizeof(struct stm_selector_s), GFP_KERNEL);
	if (!sel) {
		pr_err("No memory for new selector %s\n", name);
		return -ENOMEM;
	}

	sel_class = get_class_from_class_type(SWITCH_OBJ_CLASS_SELECTOR);
	err = switch_obj_init(&sel->obj, SWITCH_OBJ_TYPE_UNKNOWN,
			(char *)name, NULL, sel_class);
	if (err)
		goto err0;

	sel->obj.ops = &selector_ops;

	INIT_LIST_HEAD(&sel->in_pad_list);
	switch_global_get_sel_list(&sel_list);
	list_add_tail(&sel->obj.lh, sel_list);

	*selector = &sel->obj;
	pr_info("%s:%d> Selector %s(0x%p) allocated\n",
			__func__, __LINE__, name, *selector);
	return 0;

err0:
	pr_info("%s:%d> Selector %s failed(%d) to allocate\n",
			__func__, __LINE__, name, err);
	kfree(sel);
	return err;
}


/*!
 * \brief Deletes a Selector object
 *
 * \param obj: Selector object to delete
 *
 * \retval 0 Success
 */
static int selector_delete(struct switch_obj *sel_obj)
{
	int err = 0;
	struct  stm_selector_s *sel = get_selector_from_obj(sel_obj);

	if (!sel) {
		pr_err("Invalid selector obj 0x%p\n", sel_obj);
		err = -EINVAL;
		goto exit;
	}

	/*check if all input pad have been released?*/
	if (!list_empty(&sel->in_pad_list)) {
		err = -EBUSY;
		goto exit;
	}

	/*check that no pending attach is present?*/
	if (sel->sink_obj) {
		pr_err("Selector 0x%p has active connection with sink 0x%p\n",
				sel, sel->sink_obj);
		err = -EBUSY;
		goto exit;
	}
	err = switch_obj_deinit(sel_obj);
	if (err)
		return err;

	/*remove the selector obj from global list*/
	list_del(&sel_obj->lh);
	kfree(sel_obj);
exit:
	return err;
}


static int __update_sel_obj_type(struct stm_selector_s *sel)
{
	enum switch_obj_type_e type;
	int ret = 0;

	switch (sel->data_interface_type) {
	case STM_SELECTOR_TYPE_VIDEO:
		type = SWITCH_OBJ_TYPE_VIDEO;
		break;
	case STM_SELECTOR_TYPE_AUDIO:
		type = SWITCH_OBJ_TYPE_AUDIO;
		break;
	case STM_SELECTOR_TYPE_PCR:
		type = SWITCH_OBJ_TYPE_PCR;
		break;
	default:
		ret = -EINVAL;
		break;
	}

	if (!ret)
		set_obj_type(&sel->obj, type);

	return ret;
}

static int selector_set_control(struct  switch_obj *sel_obj, uint32_t control,
		const void *buf, uint32_t size)
{
	int ret = 0;
	int val = 0;
	struct stm_selector_s *sel = get_selector_from_obj(sel_obj);

	if (!sel)
		return -EINVAL;

	switch (control) {
	case STM_SELECTOR_CONTROL_DATA_INTERFACE_TYPE:
		ret = SET_CONTROL(sel->data_interface_type, buf, size);
		val = sel->data_interface_type;
		ret = __update_sel_obj_type(sel);
		break;
	default:
		pr_err("Invalid Set Control %d for Selector:0x%p\n",
				control, sel);
		ret = -EINVAL;
		break;
	}

	if (!ret)
		pr_info("%s:%d> %d control set to value %d for Selector 0x%p\n",
				__func__, __LINE__, control, val, sel);
	else
		pr_err("%d control failed to set to value %d for Selector 0x%p\n",
			control, val, sel);

	return ret;
}

static int selector_get_control(struct switch_obj *sel_obj, uint32_t control,
		void *buf, uint32_t size)
{
	int ret = 0;
	struct stm_selector_s *sel = get_selector_from_obj(sel_obj);

	if (!sel)
		return -EINVAL;


	switch (control) {
	case STM_SELECTOR_CONTROL_DATA_INTERFACE_TYPE:
		ret = GET_CONTROL(sel->data_interface_type, buf, size);
		break;
	default:
		pr_err("Invalid Get Control %d for Selector:0x%p\n",
				control, sel);
		ret = -EINVAL;
		break;
	}

	return ret;
}


static int __attach_to_data_push_sink(struct stm_selector_s *selector,
				stm_object_h sink_object,
				stm_object_h  target_type)
{
	int ret;
	char type_tag[STM_REGISTRY_MAX_TAG_SIZE];
	uint32_t iface_size;

	ret = stm_registry_get_attribute(target_type,
				STM_DATA_INTERFACE_PUSH, type_tag,
				sizeof(stm_data_interface_push_sink_t),
				&selector->data_push_interface,
				&iface_size);
	if (ret) {
		pr_err("Sink 0x%p does not support STM_DATA_INTERFACE_PUSH\n",
						sink_object);
		pr_err("Attachment between Selector 0x%p and Sink 0x%p failed(%d)\n",
						selector, sink_object, ret);
		goto exit;
	}

	ret = selector->data_push_interface.connect(&selector->obj,
			sink_object);
	if (ret) {
		pr_err("Failed(%d) to connect Selector 0x%p to push sink 0x%p\n",
				ret, selector, sink_object);
		goto exit;
	}

	if (selector->is_secure == true &&
		selector->data_push_interface.set_secure_path_status)
		ret = selector->data_push_interface.set_secure_path_status(sink_object,	true);

	if (ret) {
		pr_err("Failed(%d) to update secure path status of Selector 0x%p to push sink 0x%p\n",
				ret, selector, sink_object);
		goto error;
	}

	ret = stm_registry_add_connection(&selector->obj,
			STM_DATA_INTERFACE_PUSH, sink_object);
	if (ret) {
		pr_err("Failed to register connection %s (%d)\n",
				STM_DATA_INTERFACE_PUSH, ret);
		goto error;
	}

	return ret;
error:
	ret = selector->data_push_interface.disconnect(&selector->obj,
						sink_object);
	memset(&selector->data_push_interface,
			0,
			sizeof(selector->data_push_interface));

exit:
	return ret;
}

static int __attach_to_clk_push_sink(struct stm_selector_s *selector,
				stm_object_h sink_object,
				stm_object_h  target_type)
{
	int ret;
	char type_tag[STM_REGISTRY_MAX_TAG_SIZE];
	uint32_t iface_size;

	ret = stm_registry_get_attribute(target_type,
				STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH,
				type_tag,
				sizeof(stm_se_clock_data_point_interface_push_sink_t),
				&selector->clk_push_interface,
				&iface_size);
	if (ret) {
		pr_err("Sink 0x%p does not support STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH\n",
						sink_object);
		pr_err("Attachment between Selector 0x%p and Sink 0x%p failed(%d)\n",
						selector, sink_object, ret);
		goto exit;
	}

	ret = selector->clk_push_interface.connect(&selector->obj, sink_object);
	if (ret) {
		pr_err("Failed(%d) to connect Selector 0x%p to push sink 0x%p\n",
				ret, selector, sink_object);
		goto exit;
	}

	ret = stm_registry_add_connection(&selector->obj,
			STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH, sink_object);
	if (ret) {
		pr_err("Failed to register connection %s (%d)\n",
				STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH, ret);
		ret = selector->clk_push_interface.disconnect(&selector->obj,
							sink_object);
		memset(&selector->clk_push_interface, 0,
				sizeof(selector->clk_push_interface));
	}
exit:
	return ret;
}



static int selector_attach(struct switch_obj *sel_obj, stm_object_h sink_object)
{
	int ret = 0;
	stm_object_h target_type;
	struct stm_selector_s  *selector = get_selector_from_obj(sel_obj);
	enum switch_obj_type_e obj_type;

	if (!selector) {
		pr_err("Invalid handle\n");
		return -EINVAL;
	}

	/* Check if this selector is already attached to an external object */
	if (selector->sink_obj) {
		pr_err("selector 0x%p is already attached to sink 0x%p\n",
					selector, selector->sink_obj);
		return -EBUSY;
	}

	ret = stm_registry_get_object_type(sink_object, &target_type);
	if (ret != 0) {
		pr_err("unable to get type of target object 0x%p\n",
				sink_object);
		return ret;
	}

	obj_type = get_obj_type(sel_obj);

	if (mutex_lock_interruptible(&sel_obj->lock) != 0)
		return -EINTR;

	if (SWITCH_OBJ_TYPE_AUDIO == obj_type ||
		SWITCH_OBJ_TYPE_VIDEO == obj_type)
		ret = __attach_to_data_push_sink(selector, sink_object,
				target_type);
	else if (SWITCH_OBJ_TYPE_PCR == obj_type)
		ret = __attach_to_clk_push_sink(selector, sink_object,
				target_type);
	else
		ret = -EPERM;

	if (!ret) {
		selector->sink_obj = sink_object;
		pr_info("%s:%d> Selector 0x%p attached to sink 0x%p\n",
			__func__, __LINE__, selector, sink_object);
	}
	mutex_unlock(&sel_obj->lock);

	return ret;
}


static int __detach_to_data_push_sink(struct stm_selector_s *selector,
				stm_object_h sink_object)
{
	int ret;

	ret = selector->data_push_interface.disconnect(&selector->obj, sink_object);
	if (ret) {
		pr_err("Failed(%d) to disconnect Selector 0x%p to push sink 0x%p\n",
				ret, selector, sink_object);
		goto exit;
	}

	ret = stm_registry_remove_connection(&selector->obj,
			STM_DATA_INTERFACE_PUSH);
	if (ret) {
		pr_err("Failed to unregister connection %s (%d)\n",
				STM_DATA_INTERFACE_PUSH, ret);
		goto exit;
	}

	memset(&selector->data_push_interface, 0,
			sizeof(selector->data_push_interface));
	selector->is_secure = false;
	pr_info("%s:%d> selector 0x%p deatched from sink 0x%p\n",
			__func__, __LINE__, selector, selector->sink_obj);
exit:
	return ret;
}


static int __detach_to_clk_push_sink(struct stm_selector_s *selector,
				stm_object_h sink_object)
{
	int ret;

	ret = selector->clk_push_interface.disconnect(&selector->obj,
						sink_object);
	if (ret) {
		pr_err("Failed(%d) to disconnect Selector 0x%p to push sink 0x%p\n",
				ret, selector, sink_object);
		goto exit;
	}

	ret = stm_registry_remove_connection(&selector->obj,
			STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH);
	if (ret) {
		pr_err("Failed to unregister connection %s (%d)\n",
				STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH, ret);
		goto exit;
	}

	memset(&selector->clk_push_interface, 0,
			sizeof(selector->clk_push_interface));
	pr_info("%s:%d> selector 0x%p deatched from sink 0x%p\n",
			__func__, __LINE__, selector, selector->sink_obj);

exit:
	return ret;
}

static int selector_detach(struct switch_obj *sel_obj, stm_object_h sink_object)
{
	int ret = 0;
	enum switch_obj_type_e obj_type;
	struct stm_selector_s  *selector = get_selector_from_obj(sel_obj);

	if (!selector) {
		pr_err("Invalid handle\n");
		return -EINVAL;
	}

	if (mutex_lock_interruptible(&sel_obj->lock) != 0)
		return -EINTR;

	if (!selector->sink_obj) {
		pr_err("No active connection between selector 0x%p and sink 0x%p\n",
				sel_obj, sink_object);
		goto exit;
	}
	obj_type = get_obj_type(sel_obj);

	if (SWITCH_OBJ_TYPE_AUDIO == obj_type ||
		SWITCH_OBJ_TYPE_VIDEO == obj_type)
		ret = __detach_to_data_push_sink(selector, sink_object);
	else if (SWITCH_OBJ_TYPE_PCR == obj_type)
		ret = __detach_to_clk_push_sink(selector, sink_object);
	else
		ret = -EPERM;

	if (!ret)
		selector->sink_obj = NULL;

exit:
	mutex_unlock(&sel_obj->lock);
	return ret;
}

struct list_head *selector_get_in_pad_list(struct switch_obj *sel_obj)
{
	struct stm_selector_s *sel = get_selector_from_obj(sel_obj);

	return &sel->in_pad_list;
}


int selector_set_clock_data_point(struct switch_obj *sel_obj,
				stm_se_time_format_t time_format,
				bool new_seq, unsigned long long src_time,
				unsigned long long sys_time)
{
	int ret = 0;
	struct stm_selector_s  *selector = get_selector_from_obj(sel_obj);

	if (!selector) {
		pr_err("Invalid handle\n");
		return -EINVAL;
	}

	if (mutex_lock_interruptible(&sel_obj->lock) != 0)
		return -EINTR;

	if (selector->clk_push_interface.connect) {
		ret = selector->clk_push_interface.set_clock_data_point_data(
					selector->sink_obj,
					TIME_FORMAT_PTS, new_seq,
					src_time, sys_time);
		pr_debug("%s:%d> Pushing from SEL:0x%p to SINK:0x%p\n",
				__func__, __LINE__, sel_obj,
				selector->sink_obj);
	}

	mutex_unlock(&sel_obj->lock);

	return ret;
}

static int __calculate_byte_read(struct stm_data_block *block_list,
			uint32_t block_count)
{
	uint32_t i = 0;
	uint32_t byte_read =0;

	for (; i < block_count; i++)
		byte_read += block_list[i].len;

	return byte_read;
}

int selector_push_data(struct switch_obj *sel_obj,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks)
{
	int ret = 0;

	struct stm_selector_s  *selector = get_selector_from_obj(sel_obj);

	if (!selector) {
		pr_err("Invalid handle\n");
		return -EINVAL;
	}

	if (mutex_lock_interruptible(&sel_obj->lock) != 0)
		return -EINTR;


	if (selector->data_push_interface.connect) {
		ret = selector->data_push_interface.push_data(
					selector->sink_obj, block_list,
					block_count, data_blocks);
		*data_blocks = __calculate_byte_read(block_list, block_count);
		pr_debug("%s:%d> Pushing %d bytes from SEL:0x%p to SINK:0x%p\n",
				__func__, __LINE__, *data_blocks,
				sel_obj, selector->sink_obj);
	}
	mutex_unlock(&sel_obj->lock);

	return ret;
}

int selector_set_secure_path_status(struct switch_obj *sel_obj,
					bool secure_flag)
{
	int ret = 0;

	struct stm_selector_s  *selector = get_selector_from_obj(sel_obj);

	if (!selector) {
		pr_err("Invalid handle\n");
		return -EINVAL;
	}

	if (mutex_lock_interruptible(&sel_obj->lock) != 0)
		return -EINTR;

	selector->is_secure = secure_flag;
	if (selector->data_push_interface.set_secure_path_status)
		ret = selector->data_push_interface.set_secure_path_status(
					selector->sink_obj, secure_flag);

	mutex_unlock(&sel_obj->lock);

	return ret;
}

/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/**
 *  @File   stm_selector.c
 *  @brief
 *
 *
 *
 * */

#include "stm_selector.h"
#include "selector.h"
#include "input_pad.h"
#include "switch_global.h"
#include "switch_obj.h"

int __must_check stm_selector_open(
		const char *name,
		stm_selector_h *selector)
{
	int ret = 0;
	bool flag = false;
	struct switch_obj *sel_obj = NULL;

	ret = take_global_lock();
	if (ret)
		return ret;

	flag = is_selector_allocated(name, &sel_obj);
	if (flag == true)
		inc_selector_ref_count(sel_obj);
	else
		ret = selector_new(name, &sel_obj);

	if (!ret)
		*selector = (void *)sel_obj;

	release_global_lock();

	return ret;
}
EXPORT_SYMBOL(stm_selector_open);

int __must_check stm_selector_close(stm_selector_h selector)
{
	int ret = 0;
	struct switch_obj *sel_obj;

	ret = take_global_lock();
	if (ret)
		return -EINTR;

	ret = get_sel_obj_from_hdl(selector, &sel_obj);
	if (ret) {
		pr_err("Invalid Selector handle %p\n", selector);
		goto error;
	}

	ret = dec_selector_ref_count(sel_obj);
	/* this is the last reference to be closed
	so release the resource*/
	if (ret == 0)
		ret = sel_obj->ops->delete(sel_obj);
	else
		/*reset the return value as dec_selector_ref_count
		 * returns the current ref count*/
		ret = 0;
error:
	release_global_lock();
	return ret;
}
EXPORT_SYMBOL(stm_selector_close);



int __must_check stm_selector_attach(
		stm_selector_h selector,
		stm_object_h target)
{
	int ret = 0;
	struct switch_obj *sel_obj;

	ret = take_global_lock();
	if (ret)
		return -EINTR;

	ret = get_sel_obj_from_hdl(selector, &sel_obj);
	if (ret) {
		pr_err("Invalid Selector handle %p\n", selector);
		goto error;
	}

	ret = sel_obj->ops->attach(sel_obj, target);

error:
	release_global_lock();
	return ret;
}
EXPORT_SYMBOL(stm_selector_attach);

int __must_check stm_selector_detach(
		stm_selector_h selector,
		stm_object_h target)
{
	int ret = 0;
	struct switch_obj *sel_obj;

	ret = take_global_lock();
	if (ret)
		return -EINTR;

	ret = get_sel_obj_from_hdl(selector, &sel_obj);
	if (ret) {
		pr_err("Invalid Selector handle %p\n", selector);
		goto error;
	}

	ret = sel_obj->ops->detach(sel_obj, target);

error:
	release_global_lock();
	return ret;
}
EXPORT_SYMBOL(stm_selector_detach);

int __must_check stm_selector_set_control(
		stm_selector_h selector,
		stm_selector_ctrl_t ctrl,
		unsigned int value)
{
	int ret = 0;
	struct switch_obj *sel_obj;

	ret = take_global_lock();
	if (ret)
		return -EINTR;

	ret = get_sel_obj_from_hdl(selector, &sel_obj);
	if (ret) {
		pr_err("Invalid Selector handle %p\n", selector);
		goto error;
	}

	ret = sel_obj->ops->set_control(sel_obj, ctrl, &value, sizeof(value));

error:
	release_global_lock();
	return ret;
}
EXPORT_SYMBOL(stm_selector_set_control);

int __must_check stm_selector_get_control(
		stm_selector_h selector,
		stm_selector_ctrl_t ctrl,
		unsigned int *value)
{
	int ret = 0;
	struct switch_obj *sel_obj;

	ret = take_global_lock();
	if (ret)
		return -EINTR;

	ret = get_sel_obj_from_hdl(selector, &sel_obj);
	if (ret) {
		pr_err("Invalid Selector handle %p\n", selector);
		goto error;
	}

	ret = sel_obj->ops->get_control(sel_obj, ctrl, value, sizeof(*value));

error:
	release_global_lock();
	return ret;
}
EXPORT_SYMBOL(stm_selector_get_control);

int __must_check stm_selector_pad_new(
		stm_selector_h selector,
		stm_selector_pad_h *pad)
{
	int ret = 0;
	struct switch_obj *sel_obj;

	ret = take_global_lock();
	if (ret)
		return ret;

	ret = get_sel_obj_from_hdl(selector, &sel_obj);
	if (ret) {
		pr_err("Invalid Selector handle %p\n", selector);
		goto error;
	}

	ret = input_pad_new(sel_obj, (struct switch_obj **)pad);

error:
	release_global_lock();
	return ret;
}
EXPORT_SYMBOL(stm_selector_pad_new);

int __must_check stm_selector_pad_delete(stm_selector_pad_h pad)
{
	int ret = 0;
	struct input_pad_s *in_pad = NULL;

	ret = take_global_lock();
	if (ret)
		return -EINTR;

	ret = get_in_pad_from_hdl(pad, &in_pad);
	if (ret) {
		pr_err("Invalid PAD handle %p\n", pad);
		goto error;
	}

	ret = in_pad->obj.ops->delete(&in_pad->obj);

error:
	release_global_lock();
	return ret;
}
EXPORT_SYMBOL(stm_selector_pad_delete);


int __must_check stm_selector_associate_pad(
		const char *association_name,
		stm_selector_pad_h pad_h)
{
	int ret = 0;
	struct switch_association_s *association;
	struct input_pad_s *in_pad = NULL;

	ret = take_global_lock();
	if (ret)
		return ret;

	ret = get_in_pad_from_hdl(pad_h, &in_pad);
	/*check if this is a valid handle*/
	if (ret != 0)
		goto exit;

	association = switch_get_association_handle(association_name);
	if (!association)
		/* first association with this name*/
		ret = switch_association_new(association_name, &association);

	if (association)
		/* add this input pad to the association*/
		ret = input_pad_add_to_association(in_pad, association);

	if(!ret)
		pr_info("%s:%d> PAD:0x%p added to association:%s(0x%p)\n",
				__func__, __LINE__, in_pad,
				association_name, association);
exit:
	release_global_lock();
	return ret;
}
EXPORT_SYMBOL(stm_selector_associate_pad);

int __must_check stm_selector_pad_set_control(
		stm_selector_pad_h pad,
		stm_selector_pad_ctrl_t ctrl,
		unsigned int value)
{
	int ret = 0;
	struct input_pad_s *in_pad = NULL;

	ret = take_global_lock();
	if (ret)
		return -EINTR;

	ret = get_in_pad_from_hdl(pad, &in_pad);
	if (ret) {
		pr_err("Invalid PAD handle %p\n", pad);
		goto error;
	}
	ret = in_pad->obj.ops->set_control(&in_pad->obj, ctrl,
				&value, sizeof(value));

error:
	release_global_lock();
	return ret;
}
EXPORT_SYMBOL(stm_selector_pad_set_control);

int __must_check stm_selector_pad_get_control(
		stm_selector_pad_h pad,
		stm_selector_pad_ctrl_t ctrl,
		unsigned int *value)
{
	int ret = 0;
	struct input_pad_s *in_pad = NULL;

	ret = take_global_lock();
	if (ret)
		return -EINTR;

	ret = get_in_pad_from_hdl(pad, &in_pad);
	if (ret) {
		pr_err("Invalid PAD handle %p\n", pad);
		goto error;
	}
	ret = in_pad->obj.ops->get_control(&in_pad->obj, ctrl,
				value, sizeof(*value));
error:
	release_global_lock();
	return ret;
}
EXPORT_SYMBOL(stm_selector_pad_get_control);


int __must_check stm_selector_pad_set_start_code(
		stm_selector_pad_h pad,
		const char *start_code,
		unsigned int len)
{
	int ret = 0;
	struct input_pad_s *input_pad;

	ret = take_global_lock();
	if (ret)
		return ret;

	ret = get_in_pad_from_hdl(pad, &input_pad);
	if (ret)
		pr_err("Invalid Pad Handle\n");
	else
		input_pad_set_start_code(input_pad, start_code, len);

	release_global_lock();

	return ret;
}
EXPORT_SYMBOL(stm_selector_pad_set_start_code);

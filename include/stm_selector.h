/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/**
 *  @File   stm_selector.h
 *  @brief  Interface file to expose all the API
 *
 *
 *
 * */

#ifndef _STM_SELECTOR_H
#define _STM_SELECTOR_H

#include "stm_common.h"
#include "stm_registry.h"

typedef struct stm_object_h *stm_selector_h;
typedef struct stm_object_h *stm_selector_pad_h;

typedef enum {
#define STM_SELECTOR_TYPE_VIDEO	1
#define STM_SELECTOR_TYPE_AUDIO	2
#define STM_SELECTOR_TYPE_PCR	3
	STM_SELECTOR_CONTROL_DATA_INTERFACE_TYPE = 0x1
} stm_selector_ctrl_t;

typedef enum {
#define STM_SELECTOR_PAD_CONNECT_PASS_THROUGH_MODE 0
#define STM_SELECTOR_PAD_CONNECT_REBASE_MODE 1
#define STM_SELECTOR_PAD_CONNECT_FORCE_PASS_THROUGH_MODE 2
	STM_SELECTOR_PAD_CONTROL_CONNECT_MODE = 0x1,

#define STM_SELECTOR_PAD_REBASE_ON_PES 0x1
#define STM_SELECTOR_PAD_REBASE_INSTANTLY 0x2
#define STM_SELECTOR_PAD_REBASE_ON_START_CODE 0x4
#define STM_SELECTOR_PAD_REBASE_ON_STREAM_OFFSET 0x8
	STM_SELECTOR_PAD_CONTROL_REBASE_POLICY = 0x2,
	STM_SELECTOR_PAD_CONTROL_CACHED_UNIT_COUNT = 0x3,
	STM_SELECTOR_PAD_CONTROL_BUFFER_SIZE = 0x4,
} stm_selector_pad_ctrl_t;

typedef enum {
	STM_SELECTOR_MIN_CACHING_DONE = 0x1,
	STM_SELECTOR_MIN_CACHING_FAILED = 0x2
} stm_selector_event_t;

int __must_check stm_selector_open(
		const char *name,
		stm_selector_h *selector);
int __must_check stm_selector_close(stm_selector_h selector);

int __must_check stm_selector_attach(
		stm_selector_h selector,
		stm_object_h target);
int __must_check stm_selector_detach(
		stm_selector_h selector,
		stm_object_h target);

int __must_check stm_selector_set_control(
		stm_selector_h selector,
		stm_selector_ctrl_t ctrl,
		unsigned int value);
int __must_check stm_selector_get_control(
		stm_selector_h selector,
		stm_selector_ctrl_t ctrl,
		unsigned int *value);

int __must_check stm_selector_pad_new(
		stm_selector_h selector,
		stm_selector_pad_h *pad);

int __must_check stm_selector_pad_delete(stm_selector_pad_h pad);

int __must_check stm_selector_associate_pad(
		const char *association_name,
		stm_selector_pad_h pad);

int __must_check stm_selector_dissociate_pad(
		const char *association_name,
		stm_selector_pad_h pad);

int __must_check stm_selector_pad_set_control(
		stm_selector_pad_h pad,
		stm_selector_pad_ctrl_t ctrl,
		unsigned int value);
int __must_check stm_selector_pad_get_control(
		stm_selector_pad_h pad,
		stm_selector_pad_ctrl_t ctrl,
		unsigned int *value);

int __must_check stm_selector_pad_set_start_code(
		stm_selector_pad_h pad,
		const char *start_code,
		unsigned int len);

#endif
